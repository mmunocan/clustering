#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <output_path> <list_files>"
	exit
fi

output_path=$1
list_files=$2

mkdir -p $output_path

i=0
while IFS= read -r line
do
	set -- $line
		input_files=$1
		input_path=$2
		output_id=$3
		
		./get_matrices/WHD $input_files $input_path 2664 "$output_path/$output_id-WHD.bin" 
		./get_matrices/CHD $input_files $input_path 2664 "$output_path/$output_id-CHD.bin" 
		
		i=$(($i + 1))
done < $list_files