#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <output_path> <list_files>"
	exit
fi

output_path=$1
list_files=$2

mkdir -p $output_path

i=0
while IFS= read -r line
do
	set -- $line
		input_files=$1
		input_path=$2
		output_id=$3
		sn_31=$4
		sn_32=$5
		sn_41=$6
		sn_42=$7
		hie_whd=$8
		hie_chd=$9
		kme_whd=${10}
		kme_chd=${11}
		
		python3 get_clusters_hierarchical.py "matrices/$output_id-WHD.bin" $sn_31 "$output_path/$output_id-hierarchical-WHD-4-2-3-1.bin"
		python3 get_clusters_hierarchical.py "matrices/$output_id-CHD.bin" $sn_31 "$output_path/$output_id-hierarchical-CHD-4-2-3-1.bin"
		python3 get_clusters_hierarchical.py "matrices/$output_id-WHD.bin" $sn_32 "$output_path/$output_id-hierarchical-WHD-4-2-3-2.bin"
		python3 get_clusters_hierarchical.py "matrices/$output_id-CHD.bin" $sn_32 "$output_path/$output_id-hierarchical-CHD-4-2-3-2.bin"
		python3 get_clusters_hierarchical.py "matrices/$output_id-WHD.bin" $sn_41 "$output_path/$output_id-hierarchical-WHD-4-2-4-1.bin"
		python3 get_clusters_hierarchical.py "matrices/$output_id-CHD.bin" $sn_41 "$output_path/$output_id-hierarchical-CHD-4-2-4-1.bin"
		python3 get_clusters_hierarchical.py "matrices/$output_id-WHD.bin" $sn_42 "$output_path/$output_id-hierarchical-WHD-4-2-4-2.bin"
		python3 get_clusters_hierarchical.py "matrices/$output_id-CHD.bin" $sn_42 "$output_path/$output_id-hierarchical-CHD-4-2-4-2.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-WHD.bin" $sn_31 "$output_path/$output_id-kmedoids-WHD-4-2-3-1.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-CHD.bin" $sn_31 "$output_path/$output_id-kmedoids-CHD-4-2-3-1.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-WHD.bin" $sn_32 "$output_path/$output_id-kmedoids-WHD-4-2-3-2.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-CHD.bin" $sn_32 "$output_path/$output_id-kmedoids-CHD-4-2-3-2.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-WHD.bin" $sn_41 "$output_path/$output_id-kmedoids-WHD-4-2-4-1.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-CHD.bin" $sn_41 "$output_path/$output_id-kmedoids-CHD-4-2-4-1.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-WHD.bin" $sn_42 "$output_path/$output_id-kmedoids-WHD-4-2-4-2.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-CHD.bin" $sn_42 "$output_path/$output_id-kmedoids-CHD-4-2-4-2.bin"
		
		python3 get_clusters_hierarchical.py "matrices/$output_id-WHD.bin" $hie_whd "$output_path/$output_id-hierarchical-WHD-ideal.bin"
		python3 get_clusters_hierarchical.py "matrices/$output_id-CHD.bin" $hie_chd "$output_path/$output_id-hierarchical-CHD-ideal.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-WHD.bin" $kme_whd "$output_path/$output_id-kmedoids-WHD-ideal.bin"
		python3 get_clusters_kmedoids.py "matrices/$output_id-CHD.bin" $kme_chd "$output_path/$output_id-kmedoids-CHD-ideal.bin"
		
		i=$(($i + 1))
done < $list_files