#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <output_path> <list_files>"
	exit
fi

output_path=$1
list_files=$2

mkdir -p $output_path

i=0
while IFS= read -r line
do
	set -- $line
		input_files=$1
		input_path=$2
		output_id=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-hierarchical-WHD-snaps-4-2-3-1-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-4-2-3-1-first.bin" > "$output_path/$output_result-00"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-hierarchical-WHD-snaps-4-2-3-1-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-4-2-3-1-best.bin" > "$output_path/$output_result-01"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-hierarchical-CHD-snaps-4-2-3-1-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-4-2-3-1-first.bin" > "$output_path/$output_result-02"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-hierarchical-CHD-snaps-4-2-3-1-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-4-2-3-1-best.bin" > "$output_path/$output_result-03"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-hierarchical-WHD-snaps-4-2-3-2-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-4-2-3-2-first.bin" > "$output_path/$output_result-04"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-hierarchical-WHD-snaps-4-2-3-2-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-4-2-3-2-best.bin" > "$output_path/$output_result-05"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-hierarchical-CHD-snaps-4-2-3-2-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-4-2-3-2-first.bin" > "$output_path/$output_result-06"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-hierarchical-CHD-snaps-4-2-3-2-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-4-2-3-2-best.bin" > "$output_path/$output_result-07"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-hierarchical-WHD-snaps-4-2-4-1-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-4-2-4-1-first.bin" > "$output_path/$output_result-08"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-hierarchical-WHD-snaps-4-2-4-1-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-4-2-4-1-best.bin" > "$output_path/$output_result-09"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-hierarchical-CHD-snaps-4-2-4-1-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-4-2-4-1-first.bin" > "$output_path/$output_result-10"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-hierarchical-CHD-snaps-4-2-4-1-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-4-2-4-1-best.bin" > "$output_path/$output_result-11"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-hierarchical-WHD-snaps-4-2-4-2-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-4-2-4-2-first.bin" > "$output_path/$output_result-12"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-hierarchical-WHD-snaps-4-2-4-2-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-4-2-4-2-best.bin" > "$output_path/$output_result-13"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-hierarchical-CHD-snaps-4-2-4-2-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-4-2-4-2-first.bin" > "$output_path/$output_result-14"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-hierarchical-CHD-snaps-4-2-4-2-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-4-2-4-2-best.bin" > "$output_path/$output_result-15"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-kmedoids-WHD-snaps-4-2-3-1-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-4-2-3-1-first.bin" > "$output_path/$output_result-16"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-kmedoids-WHD-snaps-4-2-3-1-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-4-2-3-1-best.bin" > "$output_path/$output_result-17"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-kmedoids-CHD-snaps-4-2-3-1-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-4-2-3-1-first.bin" > "$output_path/$output_result-18"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-kmedoids-CHD-snaps-4-2-3-1-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-4-2-3-1-best.bin" > "$output_path/$output_result-19"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-kmedoids-WHD-snaps-4-2-3-2-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-4-2-3-2-first.bin" > "$output_path/$output_result-20"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-kmedoids-WHD-snaps-4-2-3-2-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-4-2-3-2-best.bin" > "$output_path/$output_result-21"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-kmedoids-CHD-snaps-4-2-3-2-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-4-2-3-2-first.bin" > "$output_path/$output_result-22"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-kmedoids-CHD-snaps-4-2-3-2-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-4-2-3-2-best.bin" > "$output_path/$output_result-23"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-kmedoids-WHD-snaps-4-2-4-1-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-4-2-4-1-first.bin" > "$output_path/$output_result-24"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-kmedoids-WHD-snaps-4-2-4-1-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-4-2-4-1-best.bin" > "$output_path/$output_result-25"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-kmedoids-CHD-snaps-4-2-4-1-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-4-2-4-1-first.bin" > "$output_path/$output_result-26"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-kmedoids-CHD-snaps-4-2-4-1-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-4-2-4-1-best.bin" > "$output_path/$output_result-27"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-kmedoids-WHD-snaps-4-2-4-2-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-4-2-4-2-first.bin" > "$output_path/$output_result-28"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-kmedoids-WHD-snaps-4-2-4-2-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-4-2-4-2-best.bin" > "$output_path/$output_result-29"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-kmedoids-CHD-snaps-4-2-4-2-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-4-2-4-2-first.bin" > "$output_path/$output_result-30"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-kmedoids-CHD-snaps-4-2-4-2-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-4-2-4-2-best.bin" > "$output_path/$output_result-31"
		
		
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-hierarchical-WHD-ideal-4-2-3-1-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-ideal-first.bin" > "$output_path/$output_result-32"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-hierarchical-WHD-ideal-4-2-3-1-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-ideal-best.bin" > "$output_path/$output_result-33"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-hierarchical-CHD-ideal-4-2-3-1-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-ideal-first.bin" > "$output_path/$output_result-34"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-hierarchical-CHD-ideal-4-2-3-1-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-ideal-best.bin" > "$output_path/$output_result-35"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-hierarchical-WHD-ideal-4-2-3-2-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-ideal-first.bin" > "$output_path/$output_result-36"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-hierarchical-WHD-ideal-4-2-3-2-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-ideal-best.bin" > "$output_path/$output_result-37"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-hierarchical-CHD-ideal-4-2-3-2-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-ideal-first.bin" > "$output_path/$output_result-38"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-hierarchical-CHD-ideal-4-2-3-2-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-ideal-best.bin" > "$output_path/$output_result-39"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-hierarchical-WHD-ideal-4-2-4-1-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-ideal-first.bin" > "$output_path/$output_result-40"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-hierarchical-WHD-ideal-4-2-4-1-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-ideal-best.bin" > "$output_path/$output_result-41"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-hierarchical-CHD-ideal-4-2-4-1-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-ideal-first.bin" > "$output_path/$output_result-42"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-hierarchical-CHD-ideal-4-2-4-1-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-ideal-best.bin" > "$output_path/$output_result-43"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-hierarchical-WHD-ideal-4-2-4-2-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-ideal-first.bin" > "$output_path/$output_result-44"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-hierarchical-WHD-ideal-4-2-4-2-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-WHD-ideal-best.bin" > "$output_path/$output_result-45"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-hierarchical-CHD-ideal-4-2-4-2-first.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-ideal-first.bin" > "$output_path/$output_result-46"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-hierarchical-CHD-ideal-4-2-4-2-best.tk2r" 0 112 "clusters_binaries/$output_id-hierarchical-CHD-ideal-best.bin" > "$output_path/$output_result-47"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-kmedoids-WHD-ideal-4-2-3-1-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-ideal-first.bin" > "$output_path/$output_result-48"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-kmedoids-WHD-ideal-4-2-3-1-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-ideal-best.bin" > "$output_path/$output_result-49"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-kmedoids-CHD-ideal-4-2-3-1-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-ideal-first.bin" > "$output_path/$output_result-50"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-kmedoids-CHD-ideal-4-2-3-1-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-ideal-best.bin" > "$output_path/$output_result-51"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-kmedoids-WHD-ideal-4-2-3-2-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-ideal-first.bin" > "$output_path/$output_result-52"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-kmedoids-WHD-ideal-4-2-3-2-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-ideal-best.bin" > "$output_path/$output_result-53"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-kmedoids-CHD-ideal-4-2-3-2-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-ideal-first.bin" > "$output_path/$output_result-54"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-kmedoids-CHD-ideal-4-2-3-2-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-ideal-best.bin" > "$output_path/$output_result-55"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-kmedoids-WHD-ideal-4-2-4-1-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-ideal-first.bin" > "$output_path/$output_result-56"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-kmedoids-WHD-ideal-4-2-4-1-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-ideal-best.bin" > "$output_path/$output_result-57"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-kmedoids-CHD-ideal-4-2-4-1-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-ideal-first.bin" > "$output_path/$output_result-58"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-kmedoids-CHD-ideal-4-2-4-1-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-ideal-best.bin" > "$output_path/$output_result-59"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-kmedoids-WHD-ideal-4-2-4-2-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-ideal-first.bin" > "$output_path/$output_result-60"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-kmedoids-WHD-ideal-4-2-4-2-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-WHD-ideal-best.bin" > "$output_path/$output_result-61"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-kmedoids-CHD-ideal-4-2-4-2-first.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-ideal-first.bin" > "$output_path/$output_result-62"
		./k2-raster/build/tk2_raster_cluster_encode "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-kmedoids-CHD-ideal-4-2-4-2-best.tk2r" 0 112 "clusters_binaries/$output_id-kmedoids-CHD-ideal-best.bin" > "$output_path/$output_result-63"
		
		i=$(($i + 1))
done < $list_files
