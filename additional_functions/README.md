# Additional functions

This project contains additionals functions related to clustering. These functions include a new clustering technique, computation of spatial indexs and grahics generations.

These functions are not considered in the original experimental process, but could be useful in a eventural future work.

## Libraries used

* Python: sys, numpy, sklearn.cluster, sklearn_extra.cluster, libpysal.weights, esda.moran, esda.geary, esda.getisord, sklearn.preprocessing, sklearn.metrics, matplotlib

## Details

### Computing an spatial index

Given a temporal raster, for to compute a spatial index use:

``````
python3 <index>.py <inputs_filename> <inputs_path_folder> 
``````

Where:
* index>: Spatial index name. It can be *moran*, *geary* or *getis*
* <inputs_filename> <inputs_path_folder>: temporal raster dataset

### DBSCAN clustering computarion

Also we include DBSCAN clustering. Instead of hierarchical and kmedoids, DBSCAN does not need the number of clusters; it only needs the sensibility level ($eps$).

Use:
``````
python3 DBSCAN.py  <matrix_filename> <eps> <output_filename>
``````

Where:
* <matrix_filename>: Matrix Distance Filename
* eps>: Distance between neighbors to be considered in the same cluster. We recomend a $eps=0.07$ 
* <output_filename>: New cluster filename

### Graphic Silhouette Index

Given a distance matrix, the script generate a graphic that shows the behavior of Silhouette Index testing different number of clusters between 100 and <n_rasters> in multiple of 100. Use:
``````
python3 graphic_silhouette_<strategy>.py  <matrix_filename>
``````
Where:
* strategy>: Cluster technique used. It could be *hierarchical* or *kmedoids*
* <matrix_filename>: Matrix Distance Filename

## Graphic intracluster distance (or Elbow Graphic)

Given a distance matrix, for each eventual number of cluster between 100 and <n_rasters> in multiple of 100, generate a graphic that shows the behavior of intracluster distance. It it less precise for to obtain the ideal cluster value than Silhouette Index. Use:

``````
python3 graphic_elbow_kmedoids.py  <matrix_filename>
``````
Where:
* <matrix_filename>: Matrix Distance Filename

The cluster tecnique used is kmedoids.

### Shuffle rasters

Given a temporal raster and a cluster file, shuffle the raster inside the temporal rasters, sorting according its cluster ID in a first instance and in temporal order inside each cluster. The goal is measure the improvement of building a $T$-$k^2$-raster after shufflering the rasters according its clustering.

Use:

``````
python3 shuffle_rasters.py  <input_filename> <shuffle_dataset> <output_filename>
``````
Where:
* <input_filename>: Input binary raster list
* <shuffle_dataset>: Cluster filename
* <output_filename>: Binary raster list shufflered