import sys
import numpy as np
import math

import matplotlib
import matplotlib.pyplot as plt

from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import scale
from sklearn.metrics import silhouette_score

if len(sys.argv) != 2:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> ")
	sys.exit()

matrix_filename = sys.argv[1]

# Reading the distance matrix
f = open(matrix_filename, "rb")
distance_matrix = np.fromfile(f, dtype=np.float32)
f.close()
n_rasters = (int)(math.sqrt(distance_matrix.shape[0]))
distance_matrix = np.resize(distance_matrix, (n_rasters, n_rasters))

# Computing Silhouette Index
silhouette = []
clusteres = range(100, n_rasters, 100)
for numb_clusters in clusteres:
	model = AgglomerativeClustering(metric = 'precomputed', linkage  = 'complete',n_clusters = numb_clusters)
	cluster_labels = model.fit_predict(distance_matrix)
	silhouette_avg = silhouette_score(distance_matrix, cluster_labels)
	print(str(numb_clusters) + " " + str(silhouette_avg))
	silhouette.append(silhouette_avg)

fig, ax = plt.subplots()
ax.plot(clusteres, silhouette, marker='o')
plt.grid(axis='y')
ax.set_title("Silhouette index")
ax.set_xlabel('Amount clusters')
ax.set_ylabel('Silhouette index value')
plt.tight_layout()
plt.show()