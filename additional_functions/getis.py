# Given a raster time serie, compute the Getis's G for each raster

import sys
import numpy as np
from esda.getisord import G
from libpysal.weights import W, lat2W

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <inputs_filename> <inputs_path_folder> <graphic_filename> ")
	sys.exit()

inputs_filename = sys.argv[1]
inputs_path_folder = sys.argv[2]
graphic_filename = sys.argv[3]

f = open(inputs_filename, "r")
line1 = f.readline().split()
n_rows = int(line1[0])
n_cols = int(line1[1])

raster_type = np.dtype((np.int32, (n_rows, n_cols)))
w = lat2W(n_rows, n_cols, rook=False)

getis_g = []

line1 = f.readline()
n_rasters = 0
while line1:
	raster_filename = inputs_path_folder + "/" + line1
	raster_filename = raster_filename[:-1]
	file_raster = open(raster_filename, "rb")
	
	raster = np.fromfile(file_raster, dtype=raster_type)[0]
	file_raster.close()
	
	lm = G(raster, w)
	getis_g.append(lm.G)
	n_rasters = n_rasters+1
	line1 = f.readline()
	
f.close()

raster_range = np.arange(n_rasters)

fig, ax = plt.subplots()
plt.scatter(raster_range, getis_g, color='black')
plt.title(graphic_filename)
ax.set_ylabel("G' Getis")
ax.set_xlabel('Raster ID')
ax.grid(True)

fig.set_size_inches(15, 10)

plt.tight_layout()
plt.savefig(graphic_filename)

getis_g = np.array(getis_g)
getis_mean = np.mean(getis_g)
print('{0}\t{1}'.format(inputs_filename, getis_mean))