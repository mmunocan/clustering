import sys
import numpy as np
import math

from sklearn.cluster import DBSCAN

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> <eps> <output_filename>")
	sys.exit()

matrix_filename = sys.argv[1]
eps = float(sys.argv[2])
output_filename = sys.argv[3]

# Reading the distance matrix
f = open(matrix_filename, "rb")
distance_matrix = np.fromfile(f, dtype=np.float32)
f.close()
n_rasters = (int)(math.sqrt(distance_matrix.shape[0]))
distance_matrix = np.resize(distance_matrix, (n_rasters, n_rasters))

# Normalizing the matrix
maximal = np.amax(distance_matrix)
distance_matrix = distance_matrix / maximal

# Computing the clusterization
modelo = DBSCAN(eps = eps, min_samples = 2, metric = 'precomputed')
clusteres = modelo.fit(distance_matrix).labels_

# Writing the output
clusteres = np.array(clusteres, dtype=np.int32)	
file_output = open(output_filename, "wb")
clusteres.tofile(file_output)
file_output.close()