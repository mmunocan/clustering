import sys
import numpy as np
import math

import matplotlib
import matplotlib.pyplot as plt

from sklearn_extra.cluster import KMedoids
from sklearn.preprocessing import scale
from sklearn.metrics import silhouette_score

if len(sys.argv) != 2:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> ")
	sys.exit()

matrix_filename = sys.argv[1]

# Reading the distance matrix
f = open(matrix_filename, "rb")
distance_matrix = np.fromfile(f, dtype=np.float32)
f.close()
n_rasters = (int)(math.sqrt(distance_matrix.shape[0]))
distance_matrix = np.resize(distance_matrix, (n_rasters, n_rasters))

#Computing intracluster distance
inertias = []
clusteres = range(100, n_rasters, 100)
for numb_clusters in clusteres:
	model = KMedoids(metric = 'precomputed', init  = 'k-medoids++', n_clusters = numb_clusters)
	model.fit(distance_matrix)
	print(str(numb_clusters) + " " + str(model.inertia_))
	inertias.append(model.inertia_)

fig, ax = plt.subplots()
ax.plot(clusteres, inertias, marker='o')
plt.grid(axis='y')
ax.set_title("intracluster variance")
ax.set_xlabel('Amount clusters')
ax.set_ylabel('intracluster variance value')
plt.tight_layout()
plt.show()