# Given a cluster file, make a raster permutation

import sys
import numpy as np

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <input_filename> <cluster_filename> <output_filename>")
	sys.exit()


input_filename = sys.argv[1]
cluster_filename = sys.argv[2]
output_filename = sys.argv[3]

f = open(cluster_filename, "rb")
shuffle = np.fromfile(f, dtype=np.int32)
f.close()
n_clusters = np.unique(shuffle).shape[0]
n_rasters = shuffle.shape[0]

input_file = open(input_filename, "r")
output_file = open(output_filename, "w")
line1 = input_file.readline()

output_file.write(line1)

list_names = []

for i in range(n_rasters):
	line1 = input_file.readline()
	list_names.append(line1)
input_file.close()

list_names = np.array(list_names)

for c in range(n_clusters):
	for i in range(n_rasters):
		if shuffle[i] == c:
			output_file.write(list_names[i])
	
output_file.close()