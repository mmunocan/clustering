# Clustering on rasters time series

This project applies clustering on a raster time series. For this, it generates a **cluster file** that serves as input for building a $T$-$k^2$-raster based on clustering.

The project contains three subprojects:
* matrices: Compute the distance matrices. Those distances are based on Hamming distance, that counts the cells that differs between two rasters.
* get_clusters: Given the distance matrices, compute the cluster files required for construction of the structures. Uses differents strategies and configurations.
* additional_functions: optional project that contains different additional functions as an eventual future work.

Inside each project is possible to find more information.
