#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <output_path> <list_files>"
	exit
fi

output_path=$1
list_files=$2

mkdir -p $output_path

i=0
while IFS= read -r line
do
	set -- $line
		input_files=$1
		input_path=$2
		output_id=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		python3 get_ideal_cluster_hierarchical.py "matrices/$output_id-WHD.bin" > "$output_path/$output_result-00"
		python3 get_ideal_cluster_hierarchical.py "matrices/$output_id-CHD.bin" > "$output_path/$output_result-01"
		python3 get_ideal_cluster_kmedoids.py "matrices/$output_id-WHD.bin" > "$output_path/$output_result-02"
		python3 get_ideal_cluster_kmedoids.py "matrices/$output_id-CHD.bin" > "$output_path/$output_result-03"
		
		i=$(($i + 1))
done < $list_files