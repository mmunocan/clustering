#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <output_path> <list_files>"
	exit
fi

output_path=$1
list_files=$2

mkdir -p $output_path

i=0
while IFS= read -r line
do
	set -- $line
		input_files=$1
		input_path=$2
		output_id=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		./k2-raster/build/encode_tk2r "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-k2c-3-1.tk2r" -t 100 > "$output_path/$output_result-00"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-k2c-3-2.tk2r" -t 100 > "$output_path/$output_result-01"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-k2c-4-1.tk2r" -t 100 > "$output_path/$output_result-02"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-k2c-4-2.tk2r" -t 100 > "$output_path/$output_result-03"
		
		./k2-raster/build/encode_tk2r "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-k2hc-3-1.tk2r" -t 101 > "$output_path/$output_result-04"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-k2hc-3-2.tk2r" -t 101 > "$output_path/$output_result-05"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-k2hc-4-1.tk2r" -t 101 > "$output_path/$output_result-06"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-k2hc-4-2.tk2r" -t 101 > "$output_path/$output_result-07"
		
		./k2-raster/build/encode_tk2r "$input_path/$output_id-3-1.txt" $input_path "$output_path/$output_id-htk2-3-1.tk2r" -t 112 > "$output_path/$output_result-08"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-3-2.txt" $input_path "$output_path/$output_id-htk2-3-2.tk2r" -t 112 > "$output_path/$output_result-09"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-4-1.txt" $input_path "$output_path/$output_id-htk2-4-1.tk2r" -t 112 > "$output_path/$output_result-10"
		./k2-raster/build/encode_tk2r "$input_path/$output_id-4-2.txt" $input_path "$output_path/$output_id-htk2-4-2.tk2r" -t 112 > "$output_path/$output_result-11"
		
		i=$(($i + 1))
done < $list_files