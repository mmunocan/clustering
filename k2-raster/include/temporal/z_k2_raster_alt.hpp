/*
 * Código hecho por Martita Muñoz
 */
#ifndef INCLUDED_Z_K2_RASTER_ALT
#define INCLUDED_Z_K2_RASTER_ALT

#include <k2_raster_heuristic.hpp>
#include <temporal/k2_raster_temporal_base.hpp>

template<typename k2_raster_type>
class zk2raster_alt{
public:
	typedef k2raster::k2_raster_base<>::size_type                     size_type;

public:
	size_t n_rows;
	size_t n_cols;
	size_t n_rasters;
	k2_raster_type k2raster;
public:
	
	zk2raster_alt(){}
	
	zk2raster_alt(std::string &inputs_filename, std::string &input_path_folder){
		build(inputs_filename, input_path_folder);
	}
	
	~zk2raster_alt(){
	}
	
	void build(std::string &inputs_filename, std::string &input_path_folder){
		/**************************/
        /* Reads params from file */
        /**************************/
		std::ifstream inputs_file(inputs_filename);
        assert(inputs_file.is_open() && inputs_file.good());
		
		/**************************/
        /* Count rasters          */
        /**************************/
		unsigned int n_rasters;
		std::ifstream file(inputs_filename);
		n_rasters = std::count(std::istreambuf_iterator<char>(file),
				   std::istreambuf_iterator<char>(), '\n') + 1;
		file.close();
		
		std::string file_path_input;
		size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
		k2raster::read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
		
		/*************************************/
        /* Get side and declare major vector */
        /*************************************/
		size_t side = get_side(n_rows, n_cols);
		size_t double_side = side * side;
		
		std::vector<int> values_list(n_rasters * double_side);
		size_t time = 0;
		size_t row, col;
		size_t zorder_pos, row_major_pos;
		int val;
		
		
		/*********************/
		/* Process rasters   */
		/*********************/
		while (k2raster::next_file_path(inputs_file, input_path_folder, file_path_input)) {
			std::vector<int> values;
            k2raster::read_input_data(file_path_input, n_rows, n_cols, values, 0);
			
			for(row = 0; row < side; row++){
				for(col = 0; col < side; col++){
					zorder_pos = get_zorder(row, col) + (time * double_side);
					if(row < n_rows && col < n_cols){
						row_major_pos = (row * n_cols) + col;
						val = values[row_major_pos];
					}else{
						val = 0;
					}
					values_list[zorder_pos] = val;
					
				}
				
			}
			
			time++;
		}
		inputs_file.close();
		
		/***********************/
		/* Generare k2raster   */
		/***********************/
		k2_raster_type k2raster(values_list, n_rasters, double_side, k1, k2, level_k1, plain_levels);
		this->n_rows = n_rows;
		this->n_cols = n_cols;
		this->n_rasters = n_rasters;
		this->k2raster = k2raster;
		
	}
	
	void load(std::istream& in){
		sdsl::read_member(this->n_rows, in);
		sdsl::read_member(this->n_cols, in);
		sdsl::read_member(this->n_rasters, in);
		
		this->k2raster.load(in);
	}
	
	size_t serialize(std::ostream& out, sdsl::structure_tree_node* v=nullptr, std::string name="") const {
		size_t written_bytes = 0;
		
		sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
		
		written_bytes += sdsl::write_member(this->n_rows, out, child, "n_rows");
		written_bytes += sdsl::write_member(this->n_cols, out, child, "n_cols");
		written_bytes += sdsl::write_member(this->n_rasters, out, child, "n_rasters");
		
		written_bytes += sdsl::serialize(this->k2raster, out, child, "k2raster");//this->k2raster.serialize(out, child, "k2raster");
		
		sdsl::structure_tree::add_size(child, written_bytes);
		
		return written_bytes;
	}
	
	int getCell(size_t row, size_t col, size_t time){
		//std::cout << "Inicio getCell" << std::endl;
		size_t plain_row, plain_col;
		plain_row = time;
		plain_col = get_zorder(row, col);
		//std::cout << "Tamano: [" << this->k2raster.get_n_rows() << "," << this->k2raster.get_n_cols() << "]" << std::endl;
		//std::cout << "Inicio consulta ("<< row<<","<<col<<") (" << plain_row << "," << plain_col << ")" << std::endl;
		int result = this->k2raster.get_cell(plain_row, plain_col);
		//std::cout << "Fin consulta" << std::endl;
		return result;
	}
	
	bool check(std::string &inputs_filename, std::string &input_path_folder){
		/**************************/
        /* Reads params from file */
        /**************************/
		//std::cout << "Entré al check..." << std::endl;
		std::ifstream inputs_file(inputs_filename);
        assert(inputs_file.is_open() && inputs_file.good());
		
		/**************************/
        /* Count rasters          */
        /**************************/
		unsigned int n_rasters;
		std::ifstream file(inputs_filename);
		n_rasters = std::count(std::istreambuf_iterator<char>(file),
				   std::istreambuf_iterator<char>(), '\n') + 1;
		file.close();
		
		std::string file_path_input;
		size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
		k2raster::read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
		
		/*************************************/
        /* Get side and declare major vector */
        /*************************************/
		size_t side = get_side(n_rows, n_cols);
		size_t double_side = side * side;
		std::vector<int> values_list(n_rasters * double_side);
		size_t time = 0;
		size_t row, col;
		size_t zorder_pos, row_major_pos;
		int val;
		
		/*********************/
		/* Process rasters   */
		/*********************/
		//std::cout << "Inicio del chequeo..." << std::endl;
		while (k2raster::next_file_path(inputs_file, input_path_folder, file_path_input)) {
			std::vector<int> values;
            k2raster::read_input_data(file_path_input, n_rows, n_cols, values, 0);
			
			for(row = 0; row < n_rows; row++){
				for(col = 0; col < n_cols; col++){
					row_major_pos = (row * n_cols) + col;
					if(values[row_major_pos] != getCell(row, col, time)){
						std::cout << "Error! Row: " << row << ", Col: " << col << " time: " << time << std::endl;
						std::cout << "Disk value: " << values[row_major_pos] << " != Structure value " << getCell(row, col, time) << std::endl;
						return false;
					}
					
				}
			}
			
			time++;
		}
		inputs_file.close();
		
		return true;
	}
	
private:
	
	unsigned long interleave_uint32_with_zeros(unsigned int input)  {
		unsigned long word = input;
		word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
		word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
		word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
		word = (word ^ (word << 2 )) & 0x3333333333333333;
		word = (word ^ (word << 1 )) & 0x5555555555555555;
		return word;
	}

	unsigned long get_zorder(unsigned int p, unsigned int q){
		return interleave_uint32_with_zeros(q) | (interleave_uint32_with_zeros(p) << 1);
	}

	unsigned int get_side(unsigned int rows, unsigned int cols){
		if(rows >= cols){
			return  (1 << 32-__builtin_clz(rows-1));
		}else{
			return  (1 << 32-__builtin_clz(cols-1));
		}
	}
	
};
// End zk2raster_alt class

#endif // INCLUDED_T_K2_RASTER_ALT