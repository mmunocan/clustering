
#include <temporal/at_k2_raster.hpp>

using namespace std;

typedef sdsl::int_vector<1> bitmap_type;

void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> <output_filename>\n",
           argv0);
}

template<typename tk2_raster_type>
void run(string tk2r_filename, string  output_filename){
	/*********************/
	/* READING TK2RASTER */
	/*********************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	size_t n_elements = tk2r.get_max_t();
	bitmap_type k_is_snapshot = tk2r.get_k_is_snapshot();
	
	/*********************/
	/* WRITING  BITMAPS  */
	/**********************/
	bool value;
	size_t cant_snaps = 0;
	ofstream output_file(output_filename);
	for(size_t time = 0; time < n_elements; time++){
		value = k_is_snapshot[time];
		
		if(value){
			cant_snaps++;
		}
		
		output_file.write((char *)(& value), sizeof(bool));
	}
	output_file.close();
	
	cout << tk2r_filename << ";" << cant_snaps << endl;
	
}

int main(int argc, char ** argv) {
	if(argc != 3){
	    print_help(argv[0]);
        exit(-1);
	}
	
	string tk2r_filename = argv[1];
	string output_filename = argv[2];
	
	/*****************/
	/* CHECKING TYPE */
	/************ ****/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type>(tk2r_filename, output_filename);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(tk2r_filename, output_filename);
			break;
		default:
			cout << "TK2raster type " << k2_raster_type << " cannot be processed" << endl;
			return -1;
	}
	
	return 0;
}