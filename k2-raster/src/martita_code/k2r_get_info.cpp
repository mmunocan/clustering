/*
 * Código de Martita Muñoz
 * Dado un k2-raster heurístico, imprime por pantalla información detallada respecto de su estructura
 * <k2r_filename>: nombre del archivo que contiene al k2-raster construido
 */

#include <iostream>
#include <string>
#include <k2_raster_heuristic.hpp>

typedef k2raster::k2_raster_heuristic<> k2_raster_type;
using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <k2r_filename>  \n",
           argv0);
}

int main(int argc, char ** argv){
	if(argc != 2){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string k2r_filename = argv[1];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(k2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort type;
	sdsl::read_member(type, k2raster_file);
	k2raster_file.close();
	
	if(type != k2raster::K2_RASTER_TYPE_HEURISTIC){
		cout << "El K2raster tipo " << type << " no se puede procesar con nuestro programa" << endl;
		return -1;
	}
	
	k2_raster_type k2_raster;
    sdsl::load_from_file(k2_raster, k2r_filename);
	
	k2_raster.print_info_3();
	
	return 0;
}