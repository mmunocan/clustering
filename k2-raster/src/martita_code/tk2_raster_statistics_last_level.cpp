/*
 * Código de Martita Muñoz
 * Recibe un Tk2-raster tipo 112, entrega detalles sobre el último nivel de cada k2-raster heurístico
 * que representa a cada temporal raster
 */


#include <temporal/at_k2_raster.hpp>

using namespace std;

typedef k2raster::athk2r_type  tk2_raster_type;
typedef k2raster::k2_raster_heuristic<> k2_raster_snap_type;
typedef k2raster::t_k2_raster_heuristic_log<> k2_raster_log_type;
typedef sdsl::int_vector<1> bitmap_type;

void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> \n",
           argv0);
}

int main(int argc, char ** argv){
	if(argc != 2){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	if(k2_raster_type != k2raster::ATH_K2_RASTER_TYPE){
		cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
		return -1;
	}
	
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	
	size_t n_elements = tk2r.get_max_t();
	vector<k2_raster_snap_type> snapshots = tk2r.get_snapshots();
	vector<k2_raster_log_type> logs = tk2r.get_logs();
	bitmap_type k_is_snapshot = tk2r.get_k_is_snapshot();
	
	/************************************/
	/* REVISION DE CADA TEMPORAL RASTER */
	/************************************/
	
	size_t snaps_index = 0;
	size_t log_index = 0;
	
	for(size_t time = 0; time < n_elements; time++){
		
		if(k_is_snapshot[time]){
			k2_raster_snap_type raster = snapshots[snaps_index];
			
			cout << "Raster " << time << ": SNAP" << endl;
			raster.print_info_2();
			
			snaps_index++;
		}else{
			k2_raster_log_type raster = logs[log_index];
			
			cout << "Raster " << time << ": LOG" << endl;
			raster.print_info_2();
			
			log_index++;
		}
		
	}
	
	
	return 0;
}