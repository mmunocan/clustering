/*
 * Código de Martita Muñoz 
 * Genera un at_k2_raster_cluster
 * <inputs_filename> <input_path_folder>: ubicacion del dataset de entrada
 * <output_filename>: ubicacion de la estructura de salida
 * <set_check>: decidir si se revisará el contenido de la estructura de salida
 * <type>: seleecciona el tipo de k2raster subyacente
 */

#include <temporal/at_k2_raster_cluster.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> <output_filename> <set_check> <type> <cluster_filename>\n",
           argv0);
}

template<typename temporal_k2_raster_type>
void run_encode(string inputs_filename,	string input_path_folder, string output_filename, int set_check, string cluster_filename) {
	
	auto t1 = chrono::steady_clock::now(); // Start time
	temporal_k2_raster_type k2raster(inputs_filename, input_path_folder, cluster_filename, 0);
	auto t2 = chrono::steady_clock::now(); // End time
    auto time = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();
	
	double k2_raster_size = sdsl::size_in_mega_bytes(k2raster);
	
	std::cout << output_filename << ";" << std::setprecision(2) << std::fixed << k2_raster_size << ";" << time << ";" << endl;
	
	sdsl::store_to_file(k2raster, output_filename);
	//std::string file_name = std::string(output_filename) + ".html";
    //sdsl::write_structure<sdsl::format_type::HTML_FORMAT>(k2raster, file_name);
	
	if(set_check){
		
		// Check structure in memory
		if(!k2raster.check(inputs_filename, input_path_folder, 0)){
			cout << "Test Values: FAILED!!" << endl;
		}
		
		// Load structure from disk
        std::ifstream input_file(output_filename);
        assert(input_file.is_open() && input_file.good());
        temporal_k2_raster_type k2_raster_test;
        k2_raster_test.load(input_file);
        input_file.close();
		
		// Check values
		if(!k2_raster_test.check(inputs_filename, input_path_folder, 0)){
			cout << "Test Values: FAILED!!" << endl;
		}
		
	}
}

int main(int argc, char ** argv){
	if(argc != 7){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* RECEPCION DE LOS PARAMETROS DE ENTRADA */
	/******************************************/
	string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	string output_filename = argv[3];
	int set_check = atoi(argv[4]);
	int type = atoi(argv[5]);
	string cluster_filename = argv[6];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	
	switch(type){
		case k2raster::AT_K2_RASTER_TYPE:
			run_encode<k2raster::atk2r_type>(inputs_filename, input_path_folder, output_filename, set_check, cluster_filename);
		break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run_encode<k2raster::athk2r_type>(inputs_filename, input_path_folder, output_filename, set_check, cluster_filename);
		break;
		default:
			cout << "El K2raster tipo " << type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}