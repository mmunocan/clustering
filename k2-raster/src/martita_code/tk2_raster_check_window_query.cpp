/*
 * Código de Martita Muñoz 
 * Dado un temporal raster en plains_values, un Tk2-raster equivalente y una lista de consultas del tipo
 * windows query, comprueba que ambas versiones entreguen el mismo resultado
 * 
 * ADVERTENCIA
 * 
 * Este código se encuentra en reparación y no está indexado para compilación
 */

#include <temporal/at_k2_raster.hpp>

using namespace std;

struct Query {
	int col1;
	int col2;
	int row1;
	int row2;
	int time1;
	int time2;
	int val1;
	int val2;
};

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> <tk2r_filename> <tk2r_type> <queries_filename> \n",
           argv0);
}

template<typename k2_raster_type>
void run(string inputs_filename, string input_path_folder, string tk2r_filename, string queries_filename){
	/****************************************/
	/* Obtención de la estructura en plano  */
	/****************************************/
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	// Obtención cantidad rasters
	int n_rasters;
	{
		ifstream file(inputs_filename);
		n_rasters = count(std::istreambuf_iterator<char>(file),
				   std::istreambuf_iterator<char>(), '\n') + 1;
		file.close();
	}
	
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	k2raster::read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
	
	vector<vector<int>> values(n_rasters, vector<int>(n_rows*n_cols));
	
	int i = 0;
	while (k2raster::next_file_path(inputs_file, input_path_folder, inputs_filename)) {
		k2raster::read_input_data(inputs_filename, n_rows, n_cols, values[i], 0);
		i++;
	}
	inputs_file.close();
	
	/*****************************/
	/* Obtención del tk2-raster  */
	/*****************************/
	k2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	
	/***************************************/
	/* Obtención de la lista de consultas  */
	/***************************************/
	size_t n_queries;
	ifstream file_queries(queries_filename);
	file_queries >> n_queries;
	vector<Query> list_queries;
	for(size_t i = 0; i < n_queries; i++){
		Query q;
		file_queries >> q.col1 >> q.col2 >> q.row1 >> q.row2 >> q.time1 >> q.time2 >> q.val1 >> q.val2;
		list_queries.push_back(q);
	}
	file_queries.close();
	
	/******************************/
	/* Revisión de cada consulta  */
	/******************************/
	size_t correctos = 0;
	size_t errados = 0;
	
	for(size_t i = 0; i < n_queries; i++){
		/******************************************************************/
		/* Preparación de las estructuras que almacenaran los resultados  */
		/******************************************************************/
		vector<vector<int>> result_plain(list_queries[i].time2 - list_queries[i].time1 + 1, vector<int>(((list_queries[i].col2-list_queries[i].col1+1)*(list_queries[i].row2-list_queries[i].row1+1))));
		vector<vector<int>> result_tk2r(list_queries[i].time2 - list_queries[i].time1 + 1);
		size_t n_cells_plain, n_cells_tk2r;
		
		
		/*******************************************/
		/* Obtencion de los valores de la ventana  */
		/*******************************************/
		
		n_cells_plain = 0;
		for(size_t n = list_queries[i].time1; n <= list_queries[i].time2; n++){
			unsigned int cell_result = 0;
			for(size_t r = list_queries[i].row1; r <= list_queries[i].row2; r++){
				for(size_t c = list_queries[i].col1; c <= list_queries[i].col2; c++){
					unsigned int cell_values = (r*n_cols) + c;
					result_plain[n-list_queries[i].time1][cell_result] = values[n][cell_values];
					cell_result++;
					n_cells_plain++;
				}
			}
		}
		
		n_cells_tk2r = tk2r.get_values_window(list_queries[i].col1, list_queries[i].col2, list_queries[i].row1, list_queries[i].row2, list_queries[i].time1, list_queries[i].time2, result_tk2r);
		
		/**********************************/
		/* Comparación de los resultados  */
		/**********************************/
		bool iguales = true;
		if(n_cells_plain != n_cells_tk2r){
			iguales = false;
			cout << "Error cantidad celdas" << endl;
		}
		
		for(size_t c = 0; c < list_queries[i].time2 - list_queries[i].time1 + 1; c++){
			for(size_t l = 0; l < ((list_queries[i].col2-list_queries[i].col1+1)*(list_queries[i].row2-list_queries[i].row1+1)); l++){
				if(result_plain[c][l] != result_tk2r[c][l]){
					iguales = false;
				}
			}
		}
		
		if(iguales){
			correctos++;
			cout << "Consulta " << i << " correcta " << endl;
		}else{
			errados++;
			cout << "Consulta " << i << " errada " << endl;
		}
		
	}
	
	/**************************/
	/* Informe de resultados  */
	/**************************/
	cout << n_queries << " consultas" << endl;
	cout << correctos << " correctas" << endl;
	cout << errados << " erradas" << endl; 
	
}

int main(int argc, char ** argv){
	if(argc !=6){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* Recepción de los paráetros de entrada  */
	/******************************************/
	string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	string tk2r_filename = argv[3];
	int tk2r_type = atoi(argv[4]);
	string queries_filename = argv[5];
	
	/************************************/
	/* Seleccion del tipo de k2_raster  */
	/************************************/
	switch(tk2r_type){
		case k2raster::AT_K2_RASTER_TYPE:
            run<k2raster::atk2r_type>(inputs_filename, input_path_folder, tk2r_filename, queries_filename);
            break;
        case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(inputs_filename, input_path_folder, tk2r_filename, queries_filename);
            break;
		default:
			cout << "error in K2raster type " << endl;
			exit(-1);
	}
	
	return 0;
}