/* 
 * Código de Martita Muñoz 
 * Dado un Tk2-raster cluster, 
 */


#include <temporal/at_k2_raster_cluster.hpp>

using namespace std;

typedef sdsl::int_vector<> t_values_vec;

void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> <output_filename>\n",
           argv0);
}

template<typename tk2_raster_type>
void run(string tk2r_filename, string  output_filename){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	size_t n_elements = tk2r.get_max_t();
	t_values_vec cluster_snapshot = tk2r.get_cluster_snapshot();
	
	/*****************************/
	/* ESCRITURA DE LOS BITMAPS  */
	/*****************************/
	int value;
	ofstream output_file(output_filename);
	for(size_t time = 0; time < n_elements; time++){
		value = cluster_snapshot[time];
		
		
		output_file.write((char *)(& value), sizeof(int));
	}
	output_file.close();
	
	cout << tk2r_filename << endl;
	
}

int main(int argc, char ** argv) {
	if(argc != 3){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	string output_filename = argv[2];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type>(tk2r_filename, output_filename);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(tk2r_filename, output_filename);
			break;
		default:
			cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}