/*
 * Código de Martita Muñoz 
 * Dado un ztk2raster, obtiene el valor de una celda en particular
 * <ztk2r_filename>: estructura a inspeccionar
 * <time> <row> <col>: celda a revisar
 * <type>: seleecciona el tipo de k2raster subyacente
 */

#include <temporal/at_k2_raster.hpp>
#include <temporal/z_k2_raster.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <ztk2r_filename> <time> <row> <col> <type> \n",
           argv0);
}

template<typename k2_raster_type>
void run(string ztk2r_filename, size_t time, size_t row, size_t col){
	/**************************/
	/* LECTURA DEL ZTK2RASTER */
	/**************************/
	zk2raster<k2_raster_type> zk2r;
	zk2r.load_from_disk(ztk2r_filename);
	
	/***********************************/
	/* OBTENCION DEL VALOR DE LA CELDA */
	/***********************************/
	int value = zk2r.getCell(row, col, time);
	
	cout << value << endl;
}

int main(int argc, char ** argv){
	if(argc != 6){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* RECEPCION DE LOS PARAMETROS DE ENTRADA */
	/******************************************/
	string ztk2r_filename = argv[1];
	size_t time = atoi(argv[2]);
	size_t row = atoi(argv[3]);
	size_t col = atoi(argv[4]);
	int type = atoi(argv[5]);
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	
	switch(type){
		case k2raster::K2_RASTER_TYPE:
			run<k2raster::k2_raster<>>(ztk2r_filename, time, row, col);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run<k2raster::k2_raster_plain<>>(ztk2r_filename, time, row, col);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run<k2raster::k2_raster_heuristic<>>(ztk2r_filename, time, row, col);
		break;
		default:
			cout << "El K2raster tipo " << type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}