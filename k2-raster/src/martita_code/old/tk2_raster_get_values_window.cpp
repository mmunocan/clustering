/*
 * Código de Martita Muñoz 
 * Dado un Tk2-raster, realiza la consulta get_values_windows, imprimiendo sus resultados por pantalla
 */

#include <temporal/at_k2_raster.hpp>

using namespace std;


void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> <time1> <time2> <col1> <col2> <row1> <row2>\n",
           argv0);
}

template<typename tk2_raster_type>
void run(string tk2r_filename, size_t time_low, size_t time_up, size_t row_low, size_t row_up, size_t col_low, size_t col_up){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	
	/*****************************/
	/* DESARROLLO DE LA CONSULTA */
	/*****************************/
	vector<vector<int>> result(time_up - time_low + 1);
	
	size_t n_cells = tk2r.get_values_window(col_low, col_up, row_low, row_up, time_low, time_up, result);
	
	/***************************/
	/* IMPRESION DE RESULTADOS */
	/***************************/
	cout << n_cells << " celdas fueron encontradas." << endl;
	for(size_t t = 0; t < result.size(); t++){
		cout << "[";
		
		for(size_t c = 0; c < result[t].size(); c++){
			cout << result[t][c];
			if(c != result[t].size()-1){
				cout<< ",";
			}
		}
		
		cout << "]" << endl;
	}
}


int main(int argc, char ** argv){
	if(argc != 8){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	size_t time_low = atoi(argv[2]);
	size_t time_up = atoi(argv[3]);
	size_t row_low = atoi(argv[4]);
	size_t row_up = atoi(argv[5]);
	size_t col_low = atoi(argv[6]);
	size_t col_up = atoi(argv[7]);
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type>(tk2r_filename, time_low, time_up, row_low, row_up, col_low, col_up);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(tk2r_filename, time_low, time_up, row_low, row_up, col_low, col_up);
			break;
		default:
			cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}