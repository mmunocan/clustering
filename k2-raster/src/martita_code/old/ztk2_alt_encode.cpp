/*
 * Código de Martita Muñoz 
 * Genera un z_k2_raster
 * <inputs_filename> <input_path_folder>: ubicacion del dataset de entrada
 * <output_filename>: ubicacion de la estructura de salida
 * <set_check>: decidir si se revisará el contenido de la estructura de salida
 * <type>: seleecciona el tipo de k2raster subyacente
 * 
 * ADVERTENCIA
 * 
 * Este código se encuentra en reparación y no está indexado para compilación
 */

#include <iostream>
#include <string>

#include <temporal/z_k2_raster_alt.hpp>
#include <k2_raster.hpp>
#include <k2_raster_heuristic.hpp>
#include <utils/utils_time.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> <output_filename> <set_check> <type>\n",
           argv0);
}

template<typename k2_raster_type>
void run_encode(string inputs_filename,	string input_path_folder, string output_filename, int set_check) {
	auto t1 = util::time::user::now(); // Start time
	zk2raster_alt<k2_raster_type> zk2(inputs_filename, input_path_folder);
	auto t2 = util::time::user::now(); // End time
    auto time = util::time::duration_cast<util::time::seconds>(t2-t1);
	
	sdsl::store_to_file(zk2, output_filename);
	
	double k2_raster_size = sdsl::size_in_mega_bytes(zk2);
	
	std::cout << inputs_filename << "\t" << std::setprecision(2) << std::fixed << k2_raster_size << "\t" << time << endl;
	
	if(set_check){
		
		// Check structure in memory
		if(!zk2.check(inputs_filename, input_path_folder)){
			cout << "Test Values: FAILED!!" << endl;
		}
		
		// Load structure from disk
		zk2raster_alt<k2_raster_type> zk2_test;
		sdsl::load_from_file(zk2_test, output_filename);
		
		// Check values
		if(!zk2_test.check(inputs_filename, input_path_folder)){
			cout << "Test Values: FAILED!!" << endl;
		}
		
	}
}

int main(int argc, char ** argv){
	if(argc != 6){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* Recepción de los paráetros de entrada  */
	/******************************************/
	string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	string output_filename = argv[3];
	int set_check = atoi(argv[4]);
	int type = atoi(argv[5]);
	
	/************************************/
	/* Seleccion del tipo de k2_raster  */
	/************************************/
	switch(type){
		case k2raster::K2_RASTER_TYPE:
			run_encode<k2raster::k2_raster<>>(inputs_filename, input_path_folder, output_filename, set_check);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run_encode<k2raster::k2_raster_plain<>>(inputs_filename, input_path_folder, output_filename, set_check);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run_encode<k2raster::k2_raster_heuristic<>>(inputs_filename, input_path_folder, output_filename, set_check);
		break;
		default:
			cout << "error in K2raster type " << endl;
			exit(-1);
	}
	
	return 0;
}