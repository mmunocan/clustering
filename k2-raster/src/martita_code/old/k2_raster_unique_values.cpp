/* 
 * Código de Martita Muñoz 
 * Revisa la cantidad de valores únicos que presenta un raster time serie comprimido
 * BORRABLE
 */

#include <iostream>
#include <string>

#include <k2_raster.hpp>
#include <temporal/k2_raster_temporal.hpp>
#include <temporal/t_k2_raster.hpp>
#include <temporal/at_k2_raster.hpp>
#include <temporal/k2_raster_temporal_global.hpp>


void print_help(char * argv0) {
    printf("Usage: %s <raster_filename>  \n",
           argv0);
}

int main(int argc, char ** argv) {
	if (argc < 2) {
        print_help(argv[0]);
        exit(-1);
    }
	
	/************************/
	/* Recepcion del input  */
	/************************/
	
	std::string raster_filename = argv[1];
	
	
	std::cout << "\nValores únicos\n";
	std::cout << "raster_filename: " << raster_filename << "\n";
	
	/********************************/
	/* Obtención de raster compacto */
	/********************************/
	
	// Para tk2r type 111
	//k2raster::athk2r_type raster;
	// Para tk2r type 112
	k2raster::atk2r_type  raster;
	sdsl::load_from_file(raster, raster_filename);
	size_t n_elements = raster.get_max_t();
    size_t rows = raster.get_n_rows();
	size_t cols = raster.get_n_cols();
	
	/************************/
	/* Procesamiento del raster  */
	/************************/
	
	std::vector<int> uniques;
	
	int v;
	for(size_t ras = 0; ras < n_elements; ras++) {
		for(size_t row = 0; row < rows; row++) {
			for(size_t col = 0; col < cols; col++) {
				v = raster.get_cell(row, col ,ras);
				bool encontrado = false;
				for(size_t i = 0; i < uniques.size(); i++){
					if(uniques[i] == v){
						encontrado = true;
						break;
					}
				}
				if(!encontrado){
					uniques.push_back(v);
				}
			}
		}
	}
	
	std::cout << "Valores únicos: " << uniques.size() << "\n";
	
	return 0;
}