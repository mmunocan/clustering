/* 
 * Código de Martita Muñoz 
 * Dado un temporal rastr en plains values y un Tk2-raster, comprueba que las estructuras son iguales.
 * Compara ambas estructuras celda a celda 
 * <inputs_filename> <input_path_folder>: ubicacion del dataset de entrada
 * <tk2r_filename>: nombre del archivo del Tk2-raster
 */


#include <temporal/at_k2_raster.hpp>


using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_file> <input_path_folder> <tk2r_filename> \n",
           argv0);
}

template<typename tk2_raster_type>
void run(string inputs_filename, string input_path_folder, string tk2r_filename){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	
	size_t n_elements_tk2r = tk2r.get_max_t();
	size_t n_rows_tk2r = tk2r.get_n_rows();
	size_t n_cols_tk2r = tk2r.get_n_cols();
	
	/*******************************/
	/* LECTURA DE LISTA DE RASTERS */
	/*******************************/
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	
	/**********************************/
	/* COMPARACIÓN DE LAS DIMENSIONES */
	/**********************************/
	if(n_rows != n_rows_tk2r){
		cout << "La cantidad de filas no coincide. ";
		cout << "Plains values: " << n_rows << ", ";
		cout << "Tk2-raster: " << n_rows_tk2r << ".";
		cout << endl;
		exit(-1);
	}
	if(n_cols != n_cols_tk2r){
		cout << "La cantidad de columnas no coincide. ";
		cout << "Plains values: " << n_cols << ", ";
		cout << "Tk2-raster: " << n_cols_tk2r << ".";
		cout << endl;
		exit(-1);
	}
	
	/*****************************/
	/* COMPARACIÓN CELDA A CELDA */
	/*****************************/
	string raster_input_filename, raster_output_filename;
	int value, value_tk2r;
	size_t n_elements = 0;
	while(inputs_file >> raster_input_filename){
		raster_input_filename = input_path_folder + "/" + raster_input_filename;
		
		ifstream raster_input_file(raster_input_filename);
		assert(raster_input_file.is_open() && raster_input_file.good());
		for(size_t r = 0; r < n_rows; r++){
			for(size_t c = 0; c < n_cols; c++){
				raster_input_file.read((char *)(& value), sizeof(int));
				value_tk2r = tk2r.get_cell(r, c, n_elements);
				
				if(value =! value_tk2r){
					cout << "Error! En la celda [" << n_elements << ", " << r << ", " << c << "],  ";
					cout << "plain_value: " << value << ", ";
					cout << "tk2r_value: " << value_tk2r << ", ";
					cout << endl;
					exit(-1);
				}
			}
		}
		
		raster_input_file.close();
		
		n_elements++;
	}
	inputs_file.close();
	
	
	cout << "Fin de la revisión." << endl;
	cout << "Si este es el único mensaje que vio, es porque el chequeo fue correcto." << endl;
	
}

int main(int argc, char ** argv) {
	if (argc < 4) {
        print_help(argv[0]);
        exit(-1);
    }
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string inputs_filename = argv[1];
    string input_path_folder = argv[2];
	string tk2r_filename = argv[3];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort type;
	sdsl::read_member(type, k2raster_file);
	k2raster_file.close();
	
	/************************************/
	/* Seleccion del tipo de k2_raster  */
	/************************************/
	switch(type){
		case k2raster::AT_K2_RASTER_TYPE:
            run<k2raster::atk2r_type>(inputs_filename, input_path_folder, tk2r_filename);
            break;
        case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(inputs_filename, input_path_folder, tk2r_filename);
            break;
		default:
			cout << "El TK2raster tipo " << type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}