/* 
 * Código de Martita Muñoz 
 * Lee un raster para mostrar su contenido.
 * BORRABLE
 */

#include <iostream>
#include <k2_raster.hpp>
#include <utils/utils_time.hpp>

void print_help(char * argv0) {
    printf("Usage: %s <input_file> <rows> <cols> \n",
           argv0);
}

int main(int argc, char ** argv){
	if (argc < 4) {
        print_help(argv[0]);
        exit(-1);
    }
	
	char * input_file = argv[1];
    size_t rows = atoi(argv[2]);
	size_t cols = atoi(argv[3]);
	
	/*******************/
	/* Carga de datos  */
	/*******************/
	
	std::ifstream values_file(input_file); // Open file
	assert(values_file.is_open() && values_file.good());
	int data;
	size_t n = 0;
	for (size_t r = 0; r < rows; r++) {
		for (size_t c = 0; c < cols; c++) {
			sdsl::read_member(data, values_file);
			std::cout << data << " ";
			n++;
		}
		std::cout << std::endl;
	}
	
	return 0;
}