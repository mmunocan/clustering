/* 
 * Código de Martita Muñoz
 * Realiza el proceso de programación dinámica para escoger todos los 
 * snapshots a utilizar en el temporal raster
 * <inputs_filename> <input_path_folder>: Identificación del input
 */

#include <iostream>
#include <climits>
#include <temporal/at_k2_raster.hpp>
#include <utils/utils_time.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> \n",
           argv0);
}

int main(int argc, char ** argv){
	if (argc < 3) {
        print_help(argv[0]);
        exit(-1);
    }
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
    string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	string values_filename;
	
	size_t n_rasters, n_rows, n_cols, k1, k2, level_k1, plain_levels;
	
    
	/**************************************************/
	/* Inicio de la lectura/escritura de los rasters  */
	/**************************************************/
	// Obtención cantidad rasters
	{
		std::ifstream file(inputs_filename);
		n_rasters = std::count(std::istreambuf_iterator<char>(file),
				   std::istreambuf_iterator<char>(), '\n') + 1;
		file.close();
	}
	// Obtención de los otros parámetros
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	k2raster::read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
	
	vector<vector<int>> values(n_rasters, vector<int>(n_rows*n_cols));
	size_t elem = 0;
	
	auto t1 = util::time::user::now(); // Start time
	/***************************/
	/* Lectura de cada raster  */
	/***************************/
	while (k2raster::next_file_path(inputs_file, input_path_folder, inputs_filename)) {
		k2raster::read_input_data(inputs_filename, n_rows, n_cols, values[elem], 0);
		
		elem++;
	}
	auto t2 = util::time::user::now(); // End time
    auto time_lect = util::time::duration_cast<util::time::seconds>(t2-t1);
	
	cout << "Ya cargué los datos" << endl;
	cout << "Tiempo lectura: " << time_lect << " seconds" << endl;
	
	/**********************/
	/* Cálculo snapshots  */
	/**********************/
	
	t1 = util::time::user::now(); // Start time
	vector<long> ref(n_rasters, 0);
	for(elem = 0; elem < n_rasters; elem++){
		k2raster::k2_raster<> my_raster(values[elem], n_rows, n_cols, k1, k2, level_k1, plain_levels);
		ref[elem] = sdsl::size_in_bytes(my_raster);
	}
	
	/*
	cout << "Tamaño de los snapshots: " << endl;
	for(elem = 0; elem < n_rasters; elem++){
		cout << elem << ": " << ref[elem] << endl;
	}
	*/
	
	/*****************/
	/* Cálculo logs  */
	/*****************/
	vector<int> differences(n_rows*n_cols);
	vector<vector<long>> c(n_rasters, vector<long>(n_rasters, 0));
	
	for(size_t snap = 0; snap < n_rasters; snap++){
		for(size_t log = snap+1; log < n_rasters; log++){
			for(size_t n = 0; n < n_rows*n_cols; n++){
				differences[n] = values[log][n] - values[snap][n];
			}
			k2raster::k2_raster<> my_raster(differences, n_rows, n_cols, k1, k2, level_k1, plain_levels);
			c[log][snap] = sdsl::size_in_bytes(my_raster);
		}
	}
	
	/*
	cout << "Tamaño de los logs: " << endl;
	for(size_t snap = 0; snap < n_rasters; snap++){
		for(size_t log = snap+1; log < n_rasters; log++){
			cout << snap << "<--" << log << ": " << c[log][snap] << endl;
		}
	}
	*/
	
	t2 = util::time::user::now(); 
	auto time_proc = util::time::duration_cast<util::time::seconds>(t2-t1);
	
	cout << "Tiempo procesado: " << time_proc << " seconds" << endl;
	
	/************************/
	/* Dynamic programming  */
	/************************/
	cout << "Computing M" << endl;
	vector<vector<long>> M(n_rasters, vector<long>(n_rasters, 0));
	t1 = util::time::user::now(); // Start time
	for(int i = 0; i < n_rasters; i++){
		for(int j = 0; j <= i; j++){
			if(i == j){
				// Min_{k \in [0, i-1]} M[i-1,k]
				long min = LONG_MAX;
				int rep = 0;
				for(int k = 0; k <= i-1; k++){
					if(M[i-1][k] < min){
						min = M[i-1][k];
						rep++;
					}
				}
				if(rep == 0){
					min = 0;
				}
				M[i][j] = min + ref[i];
			}else if(j < i){
				// Min_{k \in [0, j]} M[i-1,k]
				long min = LONG_MAX;
				int rep = 0;
				for(int k = 0; k < j; k++){
					if(M[i-1][k] < min){
						min = M[j-1][k];
					}
					rep++;
				}
				if(rep == 0){
					min = 0;
				}
				// \Sum_{k=j+1}^{k <= i} c[k][j]
				long sum = 0;
				for(int k = j+1; k <= i; k++){
					sum += c[k][j];
				}
				M[i][j] = min + ref[j] + sum;
			}
		}
	}
	
	/**************/
	/* Compute S  */
	/**************/
	cout << "Computing S" << endl;
	vector<size_t> S(n_rasters, 0);
	for(int i = n_rasters-1; i >= 0; i--){
		// min_{s_i \in [0,i]} M[i,s_i]
		long min = LONG_MAX;
		size_t s_min = n_rasters;
		for(size_t s_i = 0; s_i <= i; s_i++){
			if(M[i][s_i] < min){
				min = M[i][s_i];
				s_min = s_i;
			}
		}
		S[i] = s_min;
	}
	t2 = util::time::user::now(); 
	auto time_dp = util::time::duration_cast<util::time::seconds>(t2-t1);
	
	/*
	cout << "Mi tabla M: "  << endl;
	cout << "\t\t";
	for(size_t i = 0; i < n_rasters; i++){
		cout << i << "\t";
	}
	cout << endl;
	
	for(size_t i = 0; i < n_rasters; i++){
		cout << i << ":\t";
		for(size_t j = 0; j <= i; j++){
			cout << M[i][j] << "\t"; 
		}
		cout << endl;
	}
	
	cout << "Mi tabla S: "  << endl;
	for(size_t i = 0; i < n_rasters; i++){
		cout << i << ": " << S[i] << endl;
	}
	*/
	cout << "Tiempo dynamic programming: " << time_dp << " seconds" << endl;
	
	/*****************/
	/* Traster size  */
	/*****************/
	
	long size = 0;
	cout << "¿Cuál es el tamaño de mi t_k2raster?" << endl;
	for(size_t i = 0; i < n_rasters; i++){
		if(i == S[i]){	// Es un snapshot
			size += ref[i];
		}else{ // Es una ref
			// S[i]: 	snap
			// i: 		log
			// c[log][snap]
			size += c[i][S[i]];
		}
	}
	
	cout << "Tamaño total: " << size << " bytes" << endl;
	
	return 0;
}