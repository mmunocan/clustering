/*
 * Código de Martita Muñoz 
 * Mide el tiempo de consulta de check cell sobre ztk2r
 * <ztk2r_filename>: dataset de entrada
 * <n_rasters> <n_rows> <n_cols>: tamaño de la consulta
 * <query_filename>: la lista de consultas
 * <vueltas>: cantidad de vueltas precalentado
 * <type>: seleecciona el tipo de k2raster subyacente
 * 
 * ADVERTENCIA
 * 
 * Este código se encuentra en reparación y no está indexado para compilación
 */


#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>

#include <temporal/z_k2_raster_alt.hpp>
#include <k2_raster.hpp>
#include <k2_raster_heuristic.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <ztk2r_filename> <n_rasters> <n_rows> <n_cols> <query_filename>  <vueltas> <type>\n",
           argv0);
}

struct Cell {
	int instant;
	int row;
	int col;
};

int acum = 0;

template<typename k2_raster_type>
void run(string ztk2r_filename, size_t n_rasters, size_t n_rows, size_t n_cols, string query_filename, size_t vueltas){
	/*******************************/
	/* Cargar las EDCs desde disco */
	/*******************************/
	zk2raster_alt<k2_raster_type> zk2r_test;
	sdsl::load_from_file(zk2r_test, ztk2r_filename);
	
	/********************************/
	/* Cargar la lista de consultas */
	/********************************/
	vector<Cell> myCells(n_rasters * n_rows * n_cols);
	ifstream input_query(query_filename);
	int instant, row, col;
	for(size_t i = 0; i < myCells.size(); i++){
		input_query.read((char *) (& instant), sizeof(int));
		input_query.read((char *) (& row), sizeof(int));
		input_query.read((char *) (& col), sizeof(int));
		myCells[i].instant = instant;
		myCells[i].row = row;
		myCells[i].col = col;
	}
	
	/*****************************/
	/* Precalentar las consultas */
	/*****************************/
	for(size_t i = 0; i < myCells.size(); i++){
		int val_zk2r = zk2r_test.getCell(myCells[i].row, myCells[i].col, myCells[i].instant);
		acum = acum & val_zk2r;
	}
	
	/*************************/
	/* Medir tiempo getCells */
	/*************************/
	
	auto t1 = chrono::steady_clock::now();
	for(size_t k = 0; k < vueltas; k++){
		for(size_t i = 0; i < myCells.size(); i++){
			int val_zk2r = zk2r_test.getCell(myCells[i].row, myCells[i].col, myCells[i].instant);
			acum = acum & val_zk2r;
		}
	}
	auto t2 = chrono::steady_clock::now();
	auto time_get_cell_zk2r = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	time_get_cell_zk2r /= (myCells.size()*vueltas);
	
	cout << ztk2r_filename << "  " << fixed << setprecision(2) << time_get_cell_zk2r << endl;
	
}

int main(int argc, char ** argv){
	if(argc != 8){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* Recepción de los paráetros de entrada  */
	/******************************************/
	string ztk2r_filename = argv[1];
	unsigned int n_rasters = atoi(argv[2]);
	unsigned int n_rows = atoi(argv[3]);
	unsigned int n_cols = atoi(argv[4]);
	string query_filename = argv[5];
	unsigned int vueltas = atoi(argv[6]);
	int type = atoi(argv[7]);
	
	/************************************/
	/* Seleccion del tipo de k2_raster  */
	/************************************/
	switch(type){
		case k2raster::K2_RASTER_TYPE:
			run<k2raster::k2_raster<>>(ztk2r_filename, n_rasters, n_rows, n_cols, query_filename, vueltas);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run<k2raster::k2_raster_plain<>>(ztk2r_filename, n_rasters, n_rows, n_cols, query_filename, vueltas);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run<k2raster::k2_raster_heuristic<>>(ztk2r_filename, n_rasters, n_rows, n_cols, query_filename, vueltas);
		break;
		default:
			cout << "error in K2raster type " << endl;
			exit(-1);
	}
	
	return 0;
}