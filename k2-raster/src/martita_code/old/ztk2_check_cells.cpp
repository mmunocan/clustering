/*
 * Código de Martita Muñoz 
 * Chequea la correctitud de la estructura ztk2-raster
 * <inputs_filename> <input_path_folder>: ubicacion del dataset de entrada
 * <ztk2r_filename>: ubicación de la estructura ztk2-raster
 * <type>: seleecciona el tipo de k2raster subyacente
 */

#include <temporal/at_k2_raster.hpp>
#include <temporal/z_k2_raster.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> <ztk2r_filename> <type>\n",
           argv0);
}

template<typename k2_raster_type>
void run(string inputs_filename, string input_path_folder, string ztk2r_filename){
	/**************************/
	/* LECTURA DEL ZTK2RASTER */
	/**************************/
	zk2raster<k2_raster_type> zk2_test(ztk2r_filename);
	cout << std::endl << "Checking structure......." << endl;

	// Check values
	
	if(zk2_test.check(inputs_filename, input_path_folder)){
		cout << "Test Values: OK!!" << endl;
	}else{
		cout << "Test Values: FAILED!!" << endl;
	}
}

int main(int argc, char ** argv){
	if(argc != 5){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* RECEPCION DE LOS PARAMETROS DE ENTRADA */
	/******************************************/
	string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	string ztk2r_filename = argv[3];
	int type = atoi(argv[4]);
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	
	switch(type){
		case k2raster::K2_RASTER_TYPE:
			run<k2raster::k2_raster<>>(inputs_filename, input_path_folder, ztk2r_filename);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run<k2raster::k2_raster_plain<>>(inputs_filename, input_path_folder, ztk2r_filename);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run<k2raster::k2_raster_heuristic<>>(inputs_filename, input_path_folder, ztk2r_filename);
		break;
		default:
			cout << "El K2raster tipo " << type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}