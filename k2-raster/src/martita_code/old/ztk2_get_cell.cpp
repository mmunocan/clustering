/*
 * Código de Martita Muñoz 
 * Dado un ZTk2-raster y un archivo que contiene una lista de consultas, obtene el tiempo exacto que demora
 * la consulta getCell del ZTk2-raster. 
 * El tiempo está en nanosegundos.
 * El algoritmo realiza un proceso de precalentado de consultas, en donde realiza la consulta previo a la 
 * medicion del tiempo y el resultado se acumula. Esto es para que el procesador no se la salte y para que 
 * las consultas se mantengan frescas en memoria principal.
 * La lista de consultas se repite <vueltas> veces.
 * <ztk2r_filename>: dataset de entrada
 * <n_elements> <n_rows> <n_cols>: tamaño de la consulta
 * <query_filename>: la lista de consultas
 * <vueltas>: cantidad de vueltas precalentado
 * <type>: seleecciona el tipo de k2raster subyacente
 */

#include <temporal/at_k2_raster.hpp>
#include <temporal/z_k2_raster.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <ztk2r_filename> <n_elements> <n_rows> <n_cols> <query_filename> <vueltas> <type>\n",
           argv0);
}

struct Cell {
	int instant;
	int row;
	int col;
};

int acum = 0;

template<typename k2_raster_type>
void run(string ztk2r_filename, size_t n_elements, size_t n_rows, size_t n_cols, string query_filename, size_t vueltas){
	/**************************/
	/* LECTURA DEL ZTK2RASTER */
	/**************************/
	zk2raster<k2_raster_type> zk2r_test;
	zk2r_test.load_from_disk(ztk2r_filename);
	
	/************************************/
	/* LECTURA DE LA LISTA DE CONSULTAS */
	/************************************/
	vector<Cell> myCells(n_elements * n_rows * n_cols);
	ifstream input_query(query_filename);
	int instant, row, col;
	for(size_t i = 0; i < myCells.size(); i++){
		input_query.read((char *) (& instant), sizeof(int));
		input_query.read((char *) (& row), sizeof(int));
		input_query.read((char *) (& col), sizeof(int));
		myCells[i].instant = instant;
		myCells[i].row = row;
		myCells[i].col = col;
	}
	
	/*****************************/
	/* PRECALENTAR LAS CONSULTAS */
	/*****************************/
	for(size_t i = 0; i < myCells.size(); i++){
		int val_zk2r = zk2r_test.getCell(myCells[i].row, myCells[i].col, myCells[i].instant);
		acum = acum & val_zk2r;
	}
	
	/***********************************/
	/* MEDICION DEL TIEMPO DE CONSULTA */
	/***********************************/
	auto t1 = chrono::steady_clock::now();
	for(size_t k = 0; k < vueltas; k++){
		for(size_t i = 0; i < myCells.size(); i++){
			int val_zk2r = zk2r_test.getCell(myCells[i].row, myCells[i].col, myCells[i].instant);
			acum = acum & val_zk2r;
		}
	}
	auto t2 = chrono::steady_clock::now();
	auto time_get_cell_zk2r = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	time_get_cell_zk2r /= (myCells.size()*vueltas);
	
	cout << ztk2r_filename << "  " << fixed << setprecision(2) << time_get_cell_zk2r << endl;
	
}

int main(int argc, char ** argv){
	if(argc != 8){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* RECEPCION DE LOS PARAMETROS DE ENTRADA */
	/******************************************/
	string ztk2r_filename = argv[1];
	size_t n_elements = atoi(argv[2]);
	size_t n_rows = atoi(argv[3]);
	size_t n_cols = atoi(argv[4]);
	string query_filename = argv[5];
	unsigned int vueltas = atoi(argv[6]);
	int type = atoi(argv[7]);
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	
	switch(type){
		case k2raster::K2_RASTER_TYPE:
			run<k2raster::k2_raster<>>(ztk2r_filename, n_elements, n_rows, n_cols, query_filename, vueltas);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run<k2raster::k2_raster_plain<>>(ztk2r_filename, n_elements, n_rows, n_cols, query_filename, vueltas);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run<k2raster::k2_raster_heuristic<>>(ztk2r_filename, n_elements, n_rows, n_cols, query_filename, vueltas);
		break;
		default:
			cout << "El K2raster tipo " << type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}