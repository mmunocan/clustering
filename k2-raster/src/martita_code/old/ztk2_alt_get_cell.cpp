/*
 * Código de Martita Muñoz 
 * Dado un ztk2r, entregar el valor de una celda entregada por paramatro (instante, fila, columna)
 * <ztk2r_filename>: estructura a inspeccionar
 * <time_instant> <row> <col>: celda a revisar
 * <type>: seleecciona el tipo de k2raster subyacente
 * 
 * ADVERTENCIA
 * 
 * Este código se encuentra en reparación y no está indexado para compilación
 */

#include <iostream>
#include <k2_raster.hpp>
#include <k2_raster_heuristic.hpp>
#include <temporal/z_k2_raster_alt.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <ztk2r_filename> <time_instant> <row> <col> <type>\n",
           argv0);
}

template<typename k2_raster_type>
void run(string ztk2r_filename, size_t time_instant , size_t row, size_t col){
	/*****************************/
	/* Cargar datos desde disco  */
	/*****************************/
	zk2raster_alt<k2_raster_type> zk2r;
	sdsl::load_from_file(zk2r, ztk2r_filename);
	
	/************************************/
	/* Obtencion del valor de la celda  */
	/************************************/
	int value = zk2r.getCell(row, col, time_instant);
	
	cout << value << endl;
}

int main(int argc, char ** argv){
	if(argc != 6){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* Recepción de los paráetros de entrada  */
	/******************************************/
	string ztk2r_filename = argv[1];
	unsigned int time_instant = atoi(argv[2]);
	unsigned int row = atoi(argv[3]);
	unsigned int col = atoi(argv[4]);
	int type = atoi(argv[5]);
	
	/************************************/
	/* Seleccion del tipo de k2_raster  */
	/************************************/
	switch(type){
		case k2raster::K2_RASTER_TYPE:
			run<k2raster::k2_raster<>>(ztk2r_filename, time_instant , row, col);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run<k2raster::k2_raster_plain<>>(ztk2r_filename, time_instant , row, col);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run<k2raster::k2_raster_heuristic<>>(ztk2r_filename, time_instant , row, col);
		break;
		default:
			cout << "error in K2raster type " << endl;
			exit(-1);
	}
	
	return 0;
}