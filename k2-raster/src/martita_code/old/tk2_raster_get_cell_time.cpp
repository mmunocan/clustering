/*
 * Código de Martita Muñoz 
 * Dado un Tk2-raster y un archivo que contiene una lista de consultas, obtene el tiempo exacto que demora
 * la consulta getCell del Tk2-raster. 
 * El tiempo está en nanosegundos.
 * El algoritmo realiza un proceso de precalentado de consultas, en donde realiza la consulta previo a la 
 * medicion del tiempo y el resultado se acumula. Esto es para que el procesador no se la salte y para que 
 * las consultas se mantengan frescas en memoria principal.
 * La lista de consultas se repite <vueltas> veces.
 */


#include <temporal/at_k2_raster.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> <query_filename> <vueltas> \n",
           argv0);
}

struct Cell {
	int instant;
	int row;
	int col;
};

int acum = 0;

template<typename tk2_raster_type>
void run(string tk2r_filename, string query_filename, size_t vueltas){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	
	size_t n_elements = tk2r.get_max_t();
	size_t n_rows = tk2r.get_n_rows();
	size_t n_cols = tk2r.get_n_cols();
	
	/************************************/
	/* LECTURA DE LA LISTA DE CONSULTAS */
	/************************************/
	size_t cant_queries = n_elements * n_rows * n_cols;
	vector<Cell> myCells(cant_queries);
	ifstream input_query(query_filename);
	assert(input_query.is_open() && input_query.good());
	
	int instant, row, col;
	for(int i = 0; i < cant_queries; i++){
		input_query.read((char *) (& instant), sizeof(int));
		input_query.read((char *) (& row), sizeof(int));
		input_query.read((char *) (& col), sizeof(int));
		
		myCells[i].instant = instant;
		myCells[i].row = row;
		myCells[i].col = col;
	}
	
	/*****************************/
	/* PRECALENTAR LAS CONSULTAS */
	/*****************************/
	for(int i = 0; i < cant_queries; i++){
		int val_tk2r = tk2r.get_cell(myCells[i].row, myCells[i].col, myCells[i].instant);
		acum = acum & val_tk2r;
	}
	
	/***********************************/
	/* MEDICION DEL TIEMPO DE CONSULTA */
	/***********************************/
	auto t1 = chrono::steady_clock::now();
	for(size_t k = 0; k < vueltas; k++){
		for(int i = 0; i < cant_queries; i++){
			int val_tk2r = tk2r.get_cell(myCells[i].row, myCells[i].col, myCells[i].instant);
			acum = acum & val_tk2r;
		}
	}
	auto t2 = chrono::steady_clock::now();
	auto time_get_cell_tk2r = chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count();
	time_get_cell_tk2r /= (cant_queries * vueltas);
	
	cout << tk2r_filename << "  " << fixed << setprecision(2) << time_get_cell_tk2r << endl;
	
}

int main(int argc, char ** argv){
	if(argc != 3){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	string query_filename = argv[2];
	size_t vueltas = atoi(argv[3]);
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type>(tk2r_filename, query_filename, vueltas);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(tk2r_filename, query_filename, vueltas);
			break;
		default:
			cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}