/* 
 * Código de Martita Muñoz 
 * Recibe un archivo tk2r y lo muestra usando la biblioteca cimg
 */

#include <iostream>
#include <string>

#include <k2_raster.hpp>
#include <temporal/k2_raster_temporal.hpp>
#include <temporal/t_k2_raster.hpp>
#include <temporal/at_k2_raster.hpp>
#include <temporal/k2_raster_temporal_global.hpp>

#include "CImg.h"

using namespace std;
using namespace cimg_library;

void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> \n",
           argv0);
}

int main(int argc, char ** argv) {
	if (argc < 2) {
        print_help(argv[0]);
        exit(-1);
    }
	
	/******************************************/
	/* Recepción de los paráetros de entrada  */
	/******************************************/
	std::string tk2r_filename = argv[1];
	
	
	/***********************/
	/* Obtención del tk2r  */
	/***********************/
	k2raster::athk2r_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	int n_elements = tk2r.get_max_t();
    int n_rows = tk2r.get_n_rows();
	int n_cols = tk2r.get_n_cols();
	
	/*****************************/
	/* Creación de los vectores  */
	/*****************************/
	float * values = new float[n_rows*n_cols];
	CImgDisplay pantalla;
	
	/*******************************/
	/* Obtenciónd de las imagenes  */
	/*******************************/
	for(int raster = 0; raster < n_elements; raster++){
		for(int row = 0; row < n_rows; row++){
			for(int col = 0; col < n_cols; col++){
				values[row*n_cols+col] = (float) tk2r.get_cell(row, col ,raster);
			}
		}
		
		CImg<float> image(values, n_rows, n_cols, 1, 1, false);
		cout << "Imprimiendo raster " << raster << endl;
		image.print ();
		pantalla.display(image);
		pantalla.wait(1000);
		
	}
	
	
	return 0;
}