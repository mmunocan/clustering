/*
 * Código de Martita Muñoz 
 * Dado un raaster en plains values, entrega una matriz de distancia con los tamaños de cada 
 * raster representado como log.
 * La matriz resultante es de tamaño <n_elements> x <n_elements>.
 * La celda [i,j] (i != j) muestra el tamaño del raster j como log con respecto al raster i
 * La celda [i,j] (i == j) muestra el tamaño del raster j como snapshot
 * <inputs_filename>, <input_path_folder>: identifican al dataset en cuestión
 * <n_elements>: cantidad de temporal raster
 * <matrix_filename>: nombre del archivo de salida
 */

#include <temporal/at_k2_raster.hpp>

using namespace std;

typedef k2raster::k2_raster_heuristic<> k2_raster_snap_type;
typedef k2raster::t_k2_raster_heuristic_log<> k2_raster_log_type;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> <n_elements> <matrix_filename>  \n",
           argv0);
}

int main(int argc, char ** argv){
	if(argc != 5){
		print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	size_t n_elements = atoi(argv[3]);
	string matrix_filename = argv[4];
	
	/*******************************/
	/* LECTURA DE LISTA DE RASTERS */
	/*******************************/
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	k2raster::read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
	
	vector<vector<int>> all_values;
	for(size_t i = 0; i < n_elements; i++){
		vector<int> values(n_rows * n_cols);
		k2raster::next_file_path(inputs_file, input_path_folder, inputs_filename);
		k2raster::read_input_data(inputs_filename, n_rows, n_cols, values, 0);
		all_values.push_back(values);
	}
	inputs_file.close();
	
	
	/**************************/
	/* OBTENCION DE LOS SNAPS */
	/**************************/
	float * results = new float[n_elements*n_elements];
	
	k2_raster_snap_type * snaps = new k2_raster_snap_type[n_elements];
	#pragma omp parallel for
	for(size_t snap = 0; snap < n_elements; snap++){
		k2_raster_snap_type raster_snap = k2_raster_snap_type(all_values[snap], n_rows, n_cols, k1, k2, level_k1, plain_levels);
		results[snap*n_elements+snap] = 0;
		snaps[snap] = raster_snap;
	}
	
	/*************************/
	/* OBTENCION DE LOS LOGS */
	/*************************/
	#pragma omp parallel for
	for(size_t snap = 0; snap < n_elements; snap++){
		for(size_t log = snap+1; log < n_elements; log++){
			if(snap != log){
				k2_raster_log_type raster_log = k2_raster_log_type(all_values[log], snaps[snap]);
				results[snap*n_elements+log] = sdsl::size_in_bytes(raster_log);
			}
		}
		
	}
	
	/******************************/
	/* ESCRITURA DE LOS RESUTADOS */
	/******************************/
	ofstream report(matrix_filename);
	float value_float;
	for(size_t rast1 = 0; rast1 < n_elements; rast1++){
		for(size_t rast2 = 0; rast2 < n_elements; rast2++){
			value_float = results[rast1*n_elements+rast2];
			report.write((char *) (& value_float), sizeof(float));
		}
	}
	
	report.close();
	
	return 0;
}