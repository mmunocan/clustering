/*
 * Código de Martita Muñoz 
 * Chequea la correctitud de la estructura ztk2-raster
 * <inputs_filename> <input_path_folder>: ubicacion del dataset de entrada
 * <output_filename>: ubicación de la estructura ztk2-raster
 * <type>: seleecciona el tipo de k2raster subyacente
 * 
 * ADVERTENCIA
 * 
 * Este código se encuentra en reparación y no está indexado para compilación
 */

#include <iostream>
#include <string>
#include <k2_raster.hpp>
#include <k2_raster_heuristic.hpp>
#include <temporal/z_k2_raster_alt.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> <output_filename>\n",
           argv0);
}

template<typename k2_raster_type>
void run(string inputs_filename, string input_path_folder, string output_filename){
	// Load structure
	zk2raster_alt<k2_raster_type> zk2_test;
	sdsl::load_from_file(zk2_test, output_filename);
	cout << std::endl << "Checking structure......." << endl;

	// Check values
	
	if(zk2_test.check(inputs_filename, input_path_folder)){
		cout << "Test Values: OK!!" << endl;
	}else{
		cout << "Test Values: FAILED!!" << endl;
	}
}

int main(int argc, char ** argv){
	if(argc != 5){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* Recepción de los paráetros de entrada  */
	/******************************************/
	string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	string output_filename = argv[3];
	int type = atoi(argv[4]);
	
	/************************************/
	/* Seleccion del tipo de k2_raster  */
	/************************************/
	switch(type){
		case k2raster::K2_RASTER_TYPE:
			run<k2raster::k2_raster<>>(inputs_filename, input_path_folder, output_filename);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run<k2raster::k2_raster_plain<>>(inputs_filename, input_path_folder, output_filename);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run<k2raster::k2_raster_heuristic<>>(inputs_filename, input_path_folder, output_filename);
		break;
		default:
			cout << "error in K2raster type " << endl;
			exit(-1);
	}
	
	return 0;
}