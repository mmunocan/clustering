/* 
 * Código de Martita Muñoz 
 * Realiza un conjunto de operaciones estadisticas 
 * BORRABLE
 */

#include <iostream>
#include <k2_raster.hpp>

void print_help(char * argv0) {
    printf("Usage: %s <input_list> <n_element>  \n",
           argv0);
}

void get_measures(std::string values_filename, size_t rows, size_t cols){
	ushort k1 = 2;
    ushort k2 = 2;
    ushort level_k1 = 2;
    ushort plain_levels = 2;
	
	/*********************/
    /* Reads input data  */
    /*********************/
    std::ifstream values_file(values_filename); // Open file
    assert(values_file.is_open() && values_file.good());


    std::vector<int> values(rows * cols);
    size_t n = 0;
    for (size_t r = 0; r < rows; r++) {
        for (size_t c = 0; c < cols; c++) {
            sdsl::read_member(values[n], values_file);
            n++;
        }
    }
	
	/*********************/
    /* Encodes data      */
    /*********************/
	
	k2raster::k2_raster<> my_raster(values, rows, cols, k1, k2, level_k1, plain_levels);
	size_t plain_size = rows * cols * sizeof(int);
	size_t k2_raster_size = sdsl::size_in_bytes(my_raster);
    double ratio = (k2_raster_size * 100.) / (plain_size);
    std::cout << values_filename << "\t" << rows << "X" << cols << "\t" << (int)(plain_size/1024) << " KB\t" 
				<< (int)(k2_raster_size/1024) << " KB\t" << ratio << "%" << std::endl;
				
}

int main(int argc, char ** argv){
	if (argc < 3) {
        print_help(argv[0]);
        exit(-1);
    }

    std::string list_filename = argv[1];
	size_t list_size = atoi(argv[2]);
	std::string values_filename;
    size_t rows, cols;
    
	std::ifstream list_file(list_filename); // Open file
    assert(list_file.is_open() && list_file.good());
	
	for(size_t elem = 0; elem < list_size; elem++){
		list_file >> values_filename;
		list_file >> rows;
		list_file >> cols;
		get_measures(values_filename, rows, cols);
	}
	
	return 0;
}