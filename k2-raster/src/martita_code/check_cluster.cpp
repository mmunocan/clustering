/*
 * Código de Martita Muñoz 
 * Calcula el tamaño (en MB) de un grupo de datasets codificados como Tk2-raster en donde los snapshots y logs son predefinidos
 * tras la aplicación de varias técnicas de clusterización.
 * Los datasets, distancias y parámetros vienen incluídos en un archivo de configuración
 * Técnicas de clusterización recibidas: kmedoids y hierarchical
 * Técnicas de selección de centroide (snapshots) recibidas: first centroide, best centroide y artificial centroide
 */


#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <iomanip>

#include <omp.h>

#include <temporal/at_k2_raster.hpp>

using namespace std;

typedef k2raster::k2_raster_heuristic<> k2_raster_snap_type;
typedef k2raster::t_k2_raster_heuristic_log<> k2_raster_log_type;

/* 
 * Calcula la clusterización artificial. 
 * Cada cluster es identificado con un ID. Por cada cluster se crea un raster artificial definido como snapshot. El resto de los rasters 
 * del mismo cluster se define como log con respecto al raster creado.
 * Retorna el tamaño en MB de todos los rasters, tanto logs como snapshots.
 * 
 * <inputs_filename>: Nombre de archivo con los parámetros del Tk2-raster y la lista de los archivos de cada temporal raster
 * <input_path_folder>: Nombre de la ubicación en donde se encuentran los archivos de cada temporal raster
 * <n_elements>: Cantidad de temporal rasters a procesar
 * <cluster_filename>: Nombre del archivo que contiene los IDs de pertenencia a un cluster para cada <n_elements> temporal raster
 * <n_cluster>: Cantidad de clusters contenidos en <cluster_filename>
 */
double check_cluster_artificial(string inputs_filename, string input_path_folder, int n_elements, string cluster_filename, int n_cluster){
	
	/**********************************/
	/* LECTURA DE ARCHIVO DE CLUSTERS */
	/**********************************/
	ifstream cluster_file(cluster_filename);
	assert(cluster_file.is_open() && cluster_file.good());
	
	int cluster_number;
	vector<int> clustering(n_elements);			// Lista de cluster
	map<int, int> contador;						// Contador de temporal raster por cluster
	for(size_t i = 0; i < n_elements; i++){
		cluster_file.read((char *) (& cluster_number), sizeof(int));
		clustering[i] = cluster_number;
		
		// Por cada cluster, contar cuántos temporal rasters contiene
		if(contador.count(cluster_number)){
			contador[cluster_number]++;
		}else{
			contador[cluster_number] = 1;
		}
	}
	cluster_file.close();
	
	
	/*******************************/
	/* LECTURA DE LISTA DE RASTERS */
	/*******************************/
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;			// Parámetros del Tk2-raster
	k2raster::read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
	
	vector<vector<int>> all_values;									// Dataset en plain_values
	vector<vector<int>> artificial_snaps(n_cluster, vector<int>(n_rows * n_cols, 0));		// Artificial snapshots en plain_values
	vector<k2_raster_snap_type> k_artificial_snap(n_cluster);		// Artificial snapshots en Tk2-raster
	for(size_t i = 0; i < n_elements; i++){
		// Recepción del i-ésimo temporal raster
		vector<int> values(n_rows * n_cols);
		k2raster::next_file_path(inputs_file, input_path_folder, inputs_filename);
		k2raster::read_input_data(inputs_filename, n_rows, n_cols, values, 0);
		all_values.push_back(values);
		
		// Preparación de los centroides artificiales: sumas
		for(size_t c = 0; c < n_rows * n_cols; c++){
			artificial_snaps[clustering[i]][c] += values[i];
		}
		
	}
	inputs_file.close();
	
	// Preparación de los centroides artificiales : promedios
	for(size_t i = 0; i < n_cluster; i++){
		if(contador.count(i)){
			for(size_t c = 0; c < n_rows * n_cols; c++){
				artificial_snaps[i][c] /= contador[i];
			}
		}
	}
	
	
	/*******************************/
	/* MEDICION DE TAMAÑO DE SNAPS */
	/*******************************/
	double total_size = 0.0;
	double snaps_size = 0.0;
	double logs_size = 0.0;
	
	for(size_t i = 0; i < n_cluster; i++){
		if(contador.count(i)){
			k2_raster_snap_type raster_snap = k2_raster_snap_type(artificial_snaps[i], n_rows, n_cols, k1, k2, level_k1, plain_levels);
			snaps_size += sdsl::size_in_bytes(raster_snap);
			k_artificial_snap[i] = raster_snap;
		}
	}
	
	/******************************/
	/* MEDICION DE TAMAÑO DE LOGS */
	/******************************/
	for(size_t i = 0; i < n_elements; i++){
		k2_raster_log_type raster_log = k2_raster_log_type(all_values[i], k_artificial_snap[clustering[i]]);
		logs_size += sdsl::size_in_bytes(raster_log);
	}
	
	snaps_size /= (1024 * 1024);
	logs_size /= (1024 * 1024);
	total_size = snaps_size + logs_size;
	
	return total_size;
}

/* 
 * Calcula la clusterización estandar. 
 * Cada cluster es identificado con el ID del raster snapshot que representa al cluster.
 * Retorna el tamaño en MB de todos los rasters, tanto logs como snapshots.
 * 
 * <inputs_filename>: Nombre de archivo con los parámetros del Tk2-raster y la lista de los archivos de cada temporal raster
 * <input_path_folder>: Nombre de la ubicación en donde se encuentran los archivos de cada temporal raster
 * <n_elements>: Cantidad de temporal rasters a procesar
 * <cluster_filename>: Nombre del archivo que contiene los IDs de pertenencia a un cluster para cada <n_elements> temporal raster
 */
double check_cluster_size(string inputs_filename, string input_path_folder, int n_elements, string cluster_filename){
	
	/**********************************/
	/* LECTURA DE ARCHIVO DE CLUSTERS */
	/**********************************/
	ifstream cluster_file(cluster_filename);
	assert(cluster_file.is_open() && cluster_file.good());
	
	int cluster_number;
	vector<int> clustering(n_elements);
	for(size_t i = 0; i < n_elements; i++){
		cluster_file.read((char *) (& cluster_number), sizeof(int));
		clustering[i] = cluster_number;
	}
	cluster_file.close();
	
	/*******************************/
	/* LECTURA DE LISTA DE RASTERS */
	/*******************************/
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;				// Parametros del Tk2-raster
	k2raster::read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
	
	vector<vector<int>> all_values;					// Temporal rasters en plains_values
	for(size_t i = 0; i < n_elements; i++){
		vector<int> values(n_rows * n_cols);
		k2raster::next_file_path(inputs_file, input_path_folder, inputs_filename);
		k2raster::read_input_data(inputs_filename, n_rows, n_cols, values, 0);
		all_values.push_back(values);
	}
	inputs_file.close();
	
	/*******************************/
	/* MEDICION DE TAMAÑO DE SNAPS */
	/*******************************/
	double total_size = 0.0;
	double snaps_size = 0.0;
	double logs_size = 0.0;
	
	map<size_t, k2_raster_snap_type> list_snapshots;
	for(size_t i = 0; i < n_elements; i++){
		if(clustering[i] == i){
			k2_raster_snap_type raster_snap = k2_raster_snap_type(all_values[i], n_rows, n_cols, k1, k2, level_k1, plain_levels);
			float size = sdsl::size_in_bytes(raster_snap);
			snaps_size += size;
			list_snapshots[i] = raster_snap;
		}
	}
	
	/******************************/
	/* MEDICION DE TAMAÑO DE LOGS */
	/******************************/
	for(size_t i = 0; i < n_elements; i++){
		if(clustering[i] != i){
			k2_raster_log_type raster_log = k2_raster_log_type(all_values[i], list_snapshots[clustering[i]]);
			float size = sdsl::size_in_bytes(raster_log);
			logs_size += size;
		}
	}
	
	snaps_size /= (1024 * 1024);
	logs_size /= (1024 * 1024);
	total_size = snaps_size + logs_size;
	return total_size;
	
}

void print_help(char * argv0) {
    printf("Usage: %s <conf_filename>  \n",
           argv0);
}

int main(int argc, char ** argv){
	if(argc != 2){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string conf_filename = argv[1];
	
	
	ifstream conf_file(conf_filename);
	assert(conf_file.is_open() && conf_file.good());
	
	/*****************************************/
	/* RECEPCION DE LOS PARAMETROS INICIALES */
	/*****************************************/
	
	int n_elements, cant_distances, cant_datasets;
	string inputs_path_folder, inputs_extension, output_path;
	
	conf_file >> n_elements;
	conf_file >> inputs_path_folder;
	conf_file >> inputs_extension;
	conf_file >> output_path;
	
	// Tipos de distancias
	conf_file >> cant_distances;
	vector<string> distances(cant_distances);
	for(int i = 0; i < cant_distances; i++){
		conf_file >> distances[i];
	}
	
	// Datasets a utilizar
	conf_file >> cant_datasets;
	vector<string> datasets(cant_datasets);
	for(int i = 0; i < cant_datasets; i++){
		conf_file >> datasets[i];
	}
	
	// Cantidad de snapshots de los Tk2-raster originales
	vector<int> cant_snaps(cant_datasets);
	for(int i = 0; i < cant_datasets; i++){
		conf_file >> cant_snaps[i];
	}
	
	// Cantidad de clusteres ideales por tecnica, distancia y dataset
	vector<vector<int>> kmedoids_clusteres(cant_distances, vector<int>(cant_datasets));
	vector<vector<int>> hierarchical_clusteres(cant_distances, vector<int>(cant_datasets));
	
	for(int i = 0; i < cant_distances; i++){
		for(int j = 0; j < cant_datasets; j++){
			conf_file >> kmedoids_clusteres[i][j];
		}
		for(int j = 0; j < cant_datasets; j++){
			conf_file >> hierarchical_clusteres[i][j];
		}
	}
	
	conf_file.close();
	
	for(int numero_distancia = 0; numero_distancia < cant_distances; numero_distancia++){
		
		vector<vector<double>> salida(4, vector<double>(cant_datasets));
		
		/****************************/
		/* CALCULO PRIMER CENTROIDE */
		/****************************/
		
		string distance_name = distances[numero_distancia];
		cout << "Distancia " << distance_name << ": " << endl;
		cout << "First centroide: " << endl;
		#pragma omp parallel for
		for(int numero_dataset = 0; numero_dataset < cant_datasets; numero_dataset++){
			
			string dataset_name = datasets[numero_dataset];
			
			string inputs_filename = inputs_path_folder + dataset_name + inputs_extension;
			string cluster_filename;
			
			cluster_filename = output_path + distance_name + "_kmedoids_" + dataset_name + "_0_first.bin";
			salida[0][numero_dataset] = check_cluster_size(inputs_filename, inputs_path_folder, n_elements, cluster_filename);
			cluster_filename = output_path + distance_name + "_kmedoids_" + dataset_name + "_1_first.bin";
			salida[1][numero_dataset] = check_cluster_size(inputs_filename, inputs_path_folder, n_elements, cluster_filename);
			
			cluster_filename = output_path + distance_name + "_hierarchical_" + dataset_name + "_0_first.bin";
			salida[2][numero_dataset] = check_cluster_size(inputs_filename, inputs_path_folder, n_elements, cluster_filename);
			cluster_filename = output_path + distance_name + "_hierarchical_" + dataset_name + "_1_first.bin";
			salida[3][numero_dataset] = check_cluster_size(inputs_filename, inputs_path_folder, n_elements, cluster_filename);
			
		}
		
		for(int numero_dataset = 0; numero_dataset < cant_datasets; numero_dataset++){
			
			string dataset_name = datasets[numero_dataset];
			int cant_cluster_kmedoids = kmedoids_clusteres[numero_distancia][numero_dataset];
			int cant_cluster_hierarchical = hierarchical_clusteres[numero_distancia][numero_dataset];
			int cant_snaps_dataset = cant_snaps[numero_dataset];
			
			string inputs_filename = inputs_path_folder + dataset_name + inputs_extension;
			
			cout << dataset_name << "\t";
			
			cout << cant_cluster_kmedoids << "\t";
			cout << fixed << setprecision(2) << salida[0][numero_dataset] << "\t";
			cout << cant_snaps_dataset << "\t";
			cout << fixed << setprecision(2) << salida[1][numero_dataset] << "\t";
			
			cout << cant_cluster_hierarchical << "\t";
			cout << fixed << setprecision(2) << salida[2][numero_dataset] << "\t";
			cout << cant_snaps_dataset << "\t";
			cout << fixed << setprecision(2) << salida[3][numero_dataset] << "\t";
			
			cout << endl;
		}
		
		/***************************/
		/* CALCULO MEJOR CENTROIDE */
		/***************************/
		
		cout << "Best centroide: " << endl;
		#pragma omp parallel for
		for(int numero_dataset = 0; numero_dataset < cant_datasets; numero_dataset++){
			
			string dataset_name = datasets[numero_dataset];
			
			string inputs_filename = inputs_path_folder + dataset_name + inputs_extension;
			string cluster_filename;
			
			cluster_filename = output_path + distance_name + "_kmedoids_" + dataset_name + "_0_best.bin";
			salida[0][numero_dataset] = check_cluster_size(inputs_filename, inputs_path_folder, n_elements, cluster_filename);
			cluster_filename = output_path + distance_name + "_kmedoids_" + dataset_name + "_1_best.bin";
			salida[1][numero_dataset] = check_cluster_size(inputs_filename, inputs_path_folder, n_elements, cluster_filename);
			
			cluster_filename = output_path + distance_name + "_hierarchical_" + dataset_name + "_0_best.bin";
			salida[2][numero_dataset] = check_cluster_size(inputs_filename, inputs_path_folder, n_elements, cluster_filename);
			cluster_filename = output_path + distance_name + "_hierarchical_" + dataset_name + "_1_best.bin";
			salida[3][numero_dataset] = check_cluster_size(inputs_filename, inputs_path_folder, n_elements, cluster_filename);
			
		}
		
		for(int numero_dataset = 0; numero_dataset < cant_datasets; numero_dataset++){
			
			string dataset_name = datasets[numero_dataset];
			int cant_cluster_kmedoids = kmedoids_clusteres[numero_distancia][numero_dataset];
			int cant_cluster_hierarchical = hierarchical_clusteres[numero_distancia][numero_dataset];
			int cant_snaps_dataset = cant_snaps[numero_dataset];
			
			string inputs_filename = inputs_path_folder + dataset_name + inputs_extension;
			
			cout << dataset_name << "\t";
			
			cout << cant_cluster_kmedoids << "\t";
			cout << fixed << setprecision(2) << salida[0][numero_dataset] << "\t";
			cout << cant_snaps_dataset << "\t";
			cout << fixed << setprecision(2) << salida[1][numero_dataset] << "\t";
			
			cout << cant_cluster_hierarchical << "\t";
			cout << fixed << setprecision(2) << salida[2][numero_dataset] << "\t";
			cout << cant_snaps_dataset << "\t";
			cout << fixed << setprecision(2) << salida[3][numero_dataset] << "\t";
			
			cout << endl;
		}
		
		/********************************/
		/* CALCULO ARTIFICIAL CENTROIDE */
		/********************************/
		
		cout << "Artificial centroide: " << endl;
		#pragma omp parallel for
		for(int numero_dataset = 0; numero_dataset < cant_datasets; numero_dataset++){
			
			string dataset_name = datasets[numero_dataset];
			string distance_name = distances[numero_distancia];
			int cant_cluster_kmedoids = kmedoids_clusteres[numero_distancia][numero_dataset];
			int cant_cluster_hierarchical = hierarchical_clusteres[numero_distancia][numero_dataset];
			int cant_snaps_dataset = cant_snaps[numero_dataset];
			
			string inputs_filename = inputs_path_folder + dataset_name + inputs_extension;
			string cluster_filename;
			
			cluster_filename = output_path + distance_name + "_kmedoids_" + dataset_name + "_0.bin";
			salida[0][numero_dataset] = check_cluster_artificial(inputs_filename, inputs_path_folder, n_elements, cluster_filename, cant_cluster_kmedoids);
			cluster_filename = output_path + distance_name + "_kmedoids_" + dataset_name + "_1.bin";
			salida[1][numero_dataset] = check_cluster_artificial(inputs_filename, inputs_path_folder, n_elements, cluster_filename, cant_snaps_dataset);
			
			cluster_filename = output_path + distance_name + "_hierarchical_" + dataset_name + "_0.bin";
			salida[2][numero_dataset] = check_cluster_artificial(inputs_filename, inputs_path_folder, n_elements, cluster_filename, cant_cluster_hierarchical);
			cluster_filename = output_path + distance_name + "_hierarchical_" + dataset_name + "_1.bin";
			salida[3][numero_dataset] = check_cluster_artificial(inputs_filename, inputs_path_folder, n_elements, cluster_filename, cant_snaps_dataset);
			
		}
		
		for(int numero_dataset = 0; numero_dataset < cant_datasets; numero_dataset++){
			
			string dataset_name = datasets[numero_dataset];
			int cant_cluster_kmedoids = kmedoids_clusteres[numero_distancia][numero_dataset];
			int cant_cluster_hierarchical = hierarchical_clusteres[numero_distancia][numero_dataset];
			int cant_snaps_dataset = cant_snaps[numero_dataset];
			
			cout << dataset_name << "\t";
			
			cout << cant_cluster_kmedoids << "\t";
			cout << fixed << setprecision(2) << salida[0][numero_dataset] << "\t";
			cout << cant_snaps_dataset << "\t";
			cout << fixed << setprecision(2) << salida[1][numero_dataset] << "\t";
			
			cout << cant_cluster_hierarchical << "\t";
			cout << fixed << setprecision(2) << salida[2][numero_dataset] << "\t";
			cout << cant_snaps_dataset << "\t";
			cout << fixed << setprecision(2) << salida[3][numero_dataset] << "\t";
			
			cout << endl;
		}
		
	}
	
	return 0;
}