/*
 * Código de Martita Muñoz 
 * Genera un z_k2_raster
 * <inputs_filename> <input_path_folder>: ubicacion del dataset de entrada
 * <output_filename>: ubicacion de la estructura de salida
 * <set_check>: decidir si se revisará el contenido de la estructura de salida
 * <type>: seleecciona el tipo de k2raster subyacente
 */

#include <temporal/at_k2_raster.hpp>
#include <temporal/z_k2_raster.hpp>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> <output_filename> <set_check> <type> \n",
           argv0);
}

template<typename k2_raster_type>
void run_encode(string inputs_filename,	string input_path_folder, string output_filename, int set_check) {
	
	auto t1 = chrono::steady_clock::now(); // Start time
	zk2raster<k2_raster_type> zk2(inputs_filename, input_path_folder);
	auto t2 = chrono::steady_clock::now(); // End time
    auto time = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();
	
	zk2.store_in_disk(output_filename);
	
	double k2_raster_size = sdsl::size_in_mega_bytes(zk2.k2raster);
	
	std::cout << inputs_filename << ";" << std::setprecision(2) << std::fixed << k2_raster_size << ";" << time << endl;
	
	if(set_check){
		
		// Check structure in memory
		if(!zk2.check(inputs_filename, input_path_folder)){
			cout << "Test Values: FAILED!!" << endl;
		}
		
		// Load structure from disk
		zk2raster<k2_raster_type> zk2_test(output_filename);
		
		// Check values
		if(!zk2_test.check(inputs_filename, input_path_folder)){
			cout << "Test Values: FAILED!!" << endl;
		}
		
	}
}

int main(int argc, char ** argv){
	if(argc != 6){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/******************************************/
	/* RECEPCION DE LOS PARAMETROS DE ENTRADA */
	/******************************************/
	string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	string output_filename = argv[3];
	int set_check = atoi(argv[4]);
	int type = atoi(argv[5]);
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	
	switch(type){
		case k2raster::K2_RASTER_TYPE:
			run_encode<k2raster::k2_raster<>>(inputs_filename, input_path_folder, output_filename, set_check);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run_encode<k2raster::k2_raster_plain<>>(inputs_filename, input_path_folder, output_filename, set_check);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run_encode<k2raster::k2_raster_heuristic<>>(inputs_filename, input_path_folder, output_filename, set_check);
		break;
		default:
			cout << "El K2raster tipo " << type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}