/*
 * Código de Martita Muñoz
 * Recibe un Tk2-raster y obtiene datos estadísticos generales de sus valores.
 * Presenta el promedio, la cantidad de valores únicos, el elemento mayor y el elemento menor
 */

#include <temporal/at_k2_raster.hpp>

#include <climits>

using namespace std;


void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> \n",
           argv0);
}

/*
 * Función que se encarga de obtener los datos estadísticos
 */
template<typename tk2_raster_type>
void run(string tk2r_filename){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);

	size_t n_elements = tk2r.get_max_t();
	size_t n_rows = tk2r.get_n_rows();
	size_t n_cols = tk2r.get_n_cols();
	
	long suma = 0;
	int maximal = INT_MIN;
	int minimal = INT_MAX;
	map<int,size_t> freq_values;
	
	/************************************/
	/* REVISION DE CADA TEMPORAL RASTER */
	/************************************/
	
	for(size_t time = 0; time < n_elements; time++){
		for(size_t r = 0; r < n_rows; r++){
			for(size_t c = 0; c < n_cols; c++){
				int value = tk2r.get_cell(r, c, time);

				suma += value;

				if(value < minimal){
					minimal = value;
				}
				if(value > maximal){
					maximal = value;
				}

				if(freq_values.count(value)){
					freq_values[value]++;
				}else{
					freq_values[value] = 1;
				}
			}
		}
	}

	double promedio = (double)suma / (n_elements * n_rows * n_cols);
	size_t uniques = freq_values.size();

	cout << tk2r_filename << ";";
	cout << n_elements << ";" << n_rows << ";" << n_cols << ";";
	cout << fixed << setprecision(3) << promedio << ";";
	cout << minimal << ";";
	cout << maximal << ";";
	cout << uniques;
	cout << endl;
	
}

int main(int argc, char ** argv){
	if(argc != 2){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type>(tk2r_filename);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(tk2r_filename);
			break;
		default:
			cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}