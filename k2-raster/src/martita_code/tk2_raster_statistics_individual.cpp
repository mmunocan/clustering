/*
 * Código de Martita Muñoz
 * Recibe un Tk2-raster y obtiene datos estadísticos generales de cada temporal raster.
 * Muestra la cantidad de rasters snapshots y logs, tamaños individuales, entre otros
 */

#include <temporal/at_k2_raster.hpp>

using namespace std;

typedef sdsl::int_vector<1> bitmap_type;
typedef sdsl::dac_vector_dp_opt<sdsl::bit_vector, sdsl::rank_support_v5<1,1>, 3> vector_type;

void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> \n",
           argv0);
}

/*
 * Función que se encarga de obtener los datos estadísticos
 * TYPE 111
 */
template<typename tk2_raster_type, typename k2_raster_snap_type, typename k2_raster_log_type>
void run(string tk2r_filename){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);

	size_t n_elements = tk2r.get_max_t();
	vector<k2_raster_snap_type> snapshots = tk2r.get_snapshots();
	vector<k2_raster_log_type> logs = tk2r.get_logs();
	bitmap_type k_is_snapshot = tk2r.get_k_is_snapshot();
	
	double total_size = sdsl::size_in_mega_bytes(tk2r);
	
	size_t snaps_index = 0;
	size_t log_index = 0;
	double snaps_size = 0.0;
	double logs_size = 0.0;
	
	cout << tk2r_filename << endl;
	
	/************************************/
	/* REVISION DE CADA TEMPORAL RASTER */
	/************************************/
	
	for(size_t time = 0; time < n_elements; time++){
		
		if(k_is_snapshot[time]){
			k2_raster_snap_type raster = snapshots[snaps_index];
			
			size_t raster_size = sdsl::size_in_bytes(raster);
			size_t height = raster.get_k_height();
			
			cout << "ID: " << time << " \t ";
			cout << "Type: SNAP \t ";
			cout << "Raster size: " << raster_size << " [bytes] \t ";
			cout << "Height: " << height << " \t ";
			cout << endl;
			
			snaps_size += raster_size;
			
			snaps_index++;
		}else{
			k2_raster_log_type raster = logs[log_index];
			
			size_t raster_size = sdsl::size_in_bytes(raster);
			size_t height = raster.get_k_height();
			
			cout << "ID: " << time << " \t ";
			cout << "Type: LOG \t ";
			cout << "Raster size: " << raster_size << " [bytes] \t ";
			cout << "Height: " << height << " \t ";
			cout << endl;
			
			logs_size += raster_size;
			
			log_index++;
		}
		
	}
	
	snaps_size /= (1024*1024);
	logs_size /= (1024*1024);
	
	cout << fixed << setprecision(3);
	cout << "Type \t cantidad \t tamaño " << endl;
	cout << "Snaps \t " << snaps_index << " \t " << snaps_size << " [MB]" << endl;
	cout << "Logs \t " << log_index << " \t " << logs_size << " [MB]" << endl;
	cout << "Total: \t " << total_size << " [MB]" << endl;
	
}

/*
 * Función que se encarga de obtener los datos estadísticos
 * TYPE 112
 */
template<typename tk2_raster_type, typename k2_raster_snap_type, typename k2_raster_log_type>
void run_he(string tk2r_filename){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);

	size_t n_elements = tk2r.get_max_t();
	vector<k2_raster_snap_type> snapshots = tk2r.get_snapshots();
	vector<k2_raster_log_type> logs = tk2r.get_logs();
	bitmap_type k_is_snapshot = tk2r.get_k_is_snapshot();
	
	double total_size = sdsl::size_in_mega_bytes(tk2r);
	
	size_t snaps_index = 0;
	size_t log_index = 0;
	double snaps_size = 0.0;
	double logs_size = 0.0;
	
	cout << tk2r_filename << " \t ";
	
	/************************************/
	/* REVISION DE CADA TEMPORAL RASTER */
	/************************************/
	
	for(size_t time = 0; time < n_elements; time++){
		
		if(k_is_snapshot[time]){
			k2_raster_snap_type raster = snapshots[snaps_index];
			
			size_t raster_size = sdsl::size_in_bytes(raster);
			size_t height = raster.get_k_height();
			size_t k_size_word = raster.get_k_size_word();
			size_t k_n_word = raster.get_k_n_words();
			
			vector_type plains_values = raster.get_k_plain_values();
			vector_type encoded_values = raster.get_k_encoded_values();
			
			cout << "ID: " << time << " \t ";
			cout << "Type: SNAP \t ";
			cout << "Raster size: " << raster_size << " [bytes] \t ";
			cout << "Height: " << height << " \t ";
			cout << "Plains values size: " << plains_values.size() << " \t ";
			cout << "Encoded values size: " << encoded_values.size() << " \t ";
			cout << "k_size_word: " << k_size_word << " \t ";
			cout << "k_n_word: " << k_n_word << " \t ";
			cout << endl;
			
			snaps_size += raster_size;
			
			snaps_index++;
		}else{
			k2_raster_log_type raster = logs[log_index];
			
			size_t raster_size = sdsl::size_in_bytes(raster);
			size_t height = raster.get_k_height();
			size_t k_size_word = raster.get_k_size_word();
			size_t k_n_word = raster.get_k_n_words();
			
			vector_type plains_values = raster.get_k_plain_values();
			vector_type encoded_values = raster.get_k_encoded_values();
			
			cout << "ID: " << time << " \t ";
			cout << "Type: LOG \t ";
			cout << "Raster size: " << raster_size << " [bytes] \t ";
			cout << "Height: " << height << " \t ";
			cout << "Plains values size: " << plains_values.size() << " \t ";
			cout << "Encoded values size: " << encoded_values.size() << " \t ";
			cout << "k_size_word: " << k_size_word << " \t ";
			cout << "k_n_word: " << k_n_word << " \t ";
			cout << endl;
			
			logs_size += raster_size;
			
			log_index++;
		}
		
	}
	
	snaps_size /= (1024*1024);
	logs_size /= (1024*1024);
	
	cout << fixed << setprecision(3);
	cout << "Type \t cantidad \t tamaño " << endl;
	cout << "Snaps \t " << snaps_index << " \t " << snaps_size << " [MB]"<< endl;
	cout << "Logs \t " << log_index << " \t " << logs_size << " [MB]" << endl;
	cout << endl;
	cout << "Total: \t " << total_size << " [MB]"<< endl;
}

int main(int argc, char ** argv){
	if(argc != 2){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type, k2raster::k2_raster<>, k2raster::t_k2_raster_log<>>(tk2r_filename);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run_he<k2raster::athk2r_type, k2raster::k2_raster_heuristic<>, k2raster::t_k2_raster_heuristic_log<>>(tk2r_filename);
			break;
		default:
			cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}