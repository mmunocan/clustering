#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <iomanip>

#include <omp.h>

#include <temporal/at_k2_raster_cluster.hpp>

using namespace std;

typedef k2raster::k2_raster_heuristic<> k2_raster_snap_type;
typedef k2raster::t_k2_raster_heuristic_log<> k2_raster_log_type;
typedef sdsl::bit_vector bit_vector_type;
typedef sdsl::rank_support_v5<1,1> rank_1_type;
typedef sdsl::dac_vector_dp_opt<bit_vector_type, rank_1_type, 3> t_values_vec;

int main(int argc, char ** argv){
	
	
	string inputs_filename = argv[1];
	string input_path_folder = argv[2];
	string cluster_filename = argv[3];
	size_t n_elements = atoi(argv[4]);
	
	/*******************************/
	/* LECTURA DE LISTA DE RASTERS */
	/*******************************/
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;				// Parametros del Tk2-raster
	k2raster::read_params_from_file(inputs_file, n_rows, n_cols, k1, k2, level_k1, plain_levels);
	
	vector<vector<int>> all_values;					// Temporal rasters en plains_values
	for(size_t i = 0; i < n_elements; i++){
		vector<int> values(n_rows * n_cols);
		k2raster::next_file_path(inputs_file, input_path_folder, inputs_filename);
		k2raster::read_input_data(inputs_filename, n_rows, n_cols, values, 0);
		all_values.push_back(values);
	}
	inputs_file.close();
	
	
	/**********************************/
	/* LECTURA DE ARCHIVO DE CLUSTERS */
	/**********************************/
	ifstream cluster_file(cluster_filename);
	assert(cluster_file.is_open() && cluster_file.good());
	
	int cluster_number;
	vector<int> clustering;
	for(size_t i = 0; i < n_elements; i++){
		cluster_file.read((char *) (& cluster_number), sizeof(int));
		clustering.push_back(cluster_number);
	}
	cluster_file.close();
	
	vector<k2_raster_snap_type> k_snapshots;
	vector<k2_raster_log_type> k_logs;
	bit_vector_type k_is_snapshot;
    rank_1_type k_is_snapshot_rank1;
	t_values_vec cluster_snapshot;
	
	/*************************/
	/* CREACION DE SNAPSHOTS */
	/*************************/
	
	k_is_snapshot.resize(n_elements);
	cluster_snapshot = t_values_vec(clustering);
	
	for(size_t i = 0; i < n_elements; i++){
		if(clustering[i] == i){
			k2_raster_snap_type raster_snap = k2_raster_snap_type(all_values[i], n_rows, n_cols, k1, k2, level_k1, plain_levels);
			k_snapshots.push_back(raster_snap);
			k_is_snapshot[i] = 1;
		}else{
			k_is_snapshot[i] = 0;
		}
	}
	
	sdsl::util::init_support(k_is_snapshot_rank1, &k_is_snapshot);
	
	/********************/
	/* CREACION DE LOGS */
	/********************/
	for(size_t i = 0; i < n_elements; i++){
		if(clustering[i] != i){
			k2_raster_log_type raster_log = k2_raster_log_type(all_values[i], k_snapshots[k_is_snapshot_rank1(clustering[i])]);
			k_logs.push_back(raster_log);
		}
	}
	
	cout << "Cargue todo, vamos a ver los tamaños!" << endl;
	
	double k_is_snapshot_size = sdsl::size_in_bytes(k_is_snapshot) / (1024.0*1024.0);
	double k_is_snapshot_rank1_size = sdsl::size_in_bytes(k_is_snapshot_rank1) / (1024.0*1024.0);
	double cluster_snapshot_size = sdsl::size_in_bytes(cluster_snapshot) / (1024.0*1024.0);
	//double k_snapshots_size = sdsl::size_in_bytes(k_snapshots) / (1024.0*1024.0);
	//double k_logs_size = sdsl::size_in_bytes(k_logs) / (1024.0*1024.0);
	
	double k_snapshots_size = 0.0;
	for(int i = 0; i < k_snapshots.size(); i++){
		k_snapshots_size += sdsl::size_in_bytes(k_snapshots[i]);
	}
	k_snapshots_size /= (1024.0*1024.0);
	double k_logs_size = 0.0;
	for(int i = 0; i < k_logs.size(); i++){
		k_logs_size += sdsl::size_in_bytes(k_logs[i]);
	}
	k_logs_size /= (1024.0*1024.0);
	
	cout << "k_is_snapshot_size: \t" << k_is_snapshot_size << " [MB]" << endl;
	cout << "k_is_snapshot_rank1_size: \t" << k_is_snapshot_rank1_size << " [MB]" << endl;
	cout << "cluster_snapshot_size: \t" << cluster_snapshot_size << " [MB]" << endl;
	cout << "k_snapshots_size: \t" << k_snapshots_size << " [MB]" << endl;
	cout << "k_logs_size: \t" << k_logs_size << " [MB]" << endl;
	
	cout << "k_snapshots_size+k_logs_size: \t" << (k_snapshots_size+k_logs_size) << " [MB]" << endl;
	
	cout << "k_snapshots size: \t" << k_snapshots.size() << endl;
	cout << "k_logs size: \t" << k_logs.size() << endl;
	
	return 0;
}