/*
 * C�digo de Martita Mu�oz
 * Dado un Tk2-raster, obtiene la lista de frecuencias tanto de valores como de submatrices dde 8x8
 * La frecuencia de valores se almacena en un informe llamado <values_freq_filename> y los elementos 
 * est�n ordenados de acuerdo a los valores
 * La frecuencia de submatrices se almacena en un informe llamado <submatrix_freq_filename> y los 
 * elementos est�n ordenados por frecuencias
 */

#include <temporal/at_k2_raster.hpp>

#define SIZE 8

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> <values_freq_filename> <submatrix_freq_filename> \n",
           argv0);
}

bool sortbysec_submatrices(const pair<string,int> & a, const pair<string,int> & b){
	return (a.second > b.second);
}

string print_submatrix(string submatrix_str){
	int * submatrix_int = (int*)submatrix_str.c_str();
	string output = "";
	size_t size = submatrix_str.size()/sizeof(int);
	
	for(size_t i = 0; i < size; i++){
		output += "[" + to_string(submatrix_int[i]) + "]";
	}
	
	return output;
}

/*
 * Funci�n que se encarga de obtener los datos estad�sticos
 */
template<typename tk2_raster_type>
void run(string tk2r_filename, string values_freq_filename, string submatrix_freq_filename){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	size_t n_elements = tk2r.get_max_t();
	size_t n_rows = tk2r.get_n_rows();
	size_t n_cols = tk2r.get_n_cols();
	
	/***************************************/
	/* COMPUTO DE LA FRECUENCUA DE VALORES */
	/***************************************/
	map<int,size_t> freq_values;
	int value;
	for(size_t time = 0; time < n_elements; time++){
		for(size_t r = 0; r < n_rows; r++){
			for(size_t c = 0; c < n_cols; c++){
				value = tk2r.get_cell(r, c, time);
				
				if(freq_values.count(value)){
					freq_values[value]++;
				}else{
					freq_values[value] = 1;
				}
				
			}
		}
	}
	
	/*******************************************/
	/* COMPUTO DE LA FRECUENCUA DE SUBMATRICES */
	/*******************************************/
	map<string,size_t> freq_submatrix;
	size_t row_sub = floor(n_rows / SIZE);
	size_t col_sub = floor(n_cols / SIZE);
	size_t row, col;
	for(size_t time = 0; time < n_elements; time++){
		for(size_t r = 0; r < n_rows; r++){
			for(size_t c = 0; c < n_cols; c++){
				
				//Obtencion de la submatriz
				vector<int> submatrix(SIZE*SIZE, -1);
				char * submatrix_char;
				string submatrix_str;
				for(size_t i = 0; i < SIZE; i++){
					for(size_t j = 0; j < SIZE; j++){
						row = (r*SIZE) + i;
						col = (c*SIZE) + j;
						
						if(row < n_rows && col < n_cols){
							value = tk2r.get_cell(row, col, time);
							submatrix[i*SIZE + j] = value;
						}
					}
				}
				
				submatrix_char = (char*)submatrix.data();
				submatrix_str.assign(submatrix_char, submatrix_char + SIZE*SIZE*sizeof(int));
				
				if(freq_submatrix.count(submatrix_str)){
					freq_submatrix[submatrix_str]++;
				}else{
					freq_submatrix[submatrix_str] = 1;
				}
				
			}
		}
	}
	
	/*******************************************/
	/* ORDERNAR LAS SUBMATRICES POR FRECUENCIA */
	/*******************************************/
	vector<pair<string, size_t>> orderer_submatrix;
	for (auto const& it : freq_submatrix){
		orderer_submatrix.push_back(make_pair(it.first, it.second));
	}
	sort(orderer_submatrix.begin(), orderer_submatrix.end(), sortbysec_submatrices);
	
	/***********************************************/
	/* ESRIBIR INFORME DE LA FRECUENCIA DE VALORES */
	/***********************************************/
	ofstream values_freq_file(values_freq_filename);
	
	for (auto const& it : freq_values){
		values_freq_file << it.first << " " << it.second << endl;
	}
	
	values_freq_file.close();
	
	/***************************************************/
	/* ESRIBIR INFORME DE LA FRECUENCIA DE SUBMATRICES */
	/***************************************************/
	ofstream submatrix_freq_file(submatrix_freq_filename);
	
	for (auto const& it : orderer_submatrix){
		submatrix_freq_file << print_submatrix(it.first) << " " << it.second << endl;
	}
	
	submatrix_freq_file.close();
}

int main(int argc, char ** argv){
	if (argc != 4) {
        print_help(argv[0]);
        exit(-1);
    }
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	string values_freq_filename = argv[2];
	string submatrix_freq_filename = argv[3];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type>(tk2r_filename, values_freq_filename, submatrix_freq_filename);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(tk2r_filename, values_freq_filename, submatrix_freq_filename);
			break;
		default:
			cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}