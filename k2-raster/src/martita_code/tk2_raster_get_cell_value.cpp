/*
 * Código de Martita Muñoz 
 * Dado un tk2r heurísivo, entregar el valor de una celda entregada por paramatro (instante, fila, columna)
 * <tk2r_filename>: estructura a inspeccionar
 * <time> <row> <col>: celda a revisar
 */

#include <temporal/at_k2_raster.hpp>

using namespace std;


void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> <time> <row> <col> \n",
           argv0);
}

template<typename tk2_raster_type>
void run(string tk2r_filename, size_t time, size_t row, size_t col){
	/*************************/
	/* LECTURA DEL TK2RASTER */
	/*************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	
	/***********************************/
	/* OBTENCION DEL VALOR DE LA CELDA */
	/***********************************/
	int value = tk2r.get_cell(row, col, time);
	cout << value << endl;
}


int main(int argc, char ** argv){
	if(argc != 5){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	size_t time = atoi(argv[2]);
	size_t row = atoi(argv[3]);
	size_t col = atoi(argv[4]);
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type>(tk2r_filename, time, row, col);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type>(tk2r_filename, time, row, col);
			break;
		default:
			cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}