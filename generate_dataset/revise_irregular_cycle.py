import sys
import numpy as np

if len(sys.argv) != 3:
	print("ERROR! USE " + sys.argv[0] + " <inputs_filename> <inputs_path> ")
	sys.exit()

inputs_filename = sys.argv[1]
inputs_path = sys.argv[2]

f = open(inputs_filename, "r")
line1 = f.readline().split()
n_rows = int(line1[0])
n_cols = int(line1[1])

raster_type = np.dtype((np.int32, n_cols * n_rows))
cycle = []
cycles_sizes = []

line1 = f.readline()
cycle_size = 0
n = 1
while line1:
	raster_filename = inputs_path + "/" + line1
	raster_filename = raster_filename[:-1]
	file_raster = open(raster_filename, "rb")
	
	raster = np.fromfile(file_raster, dtype=raster_type)[0]
	file_raster.close()
	
	if len(cycle) == 0:
		# Primer raster, caso base
		cycle.append(raster)
		cycle_size = 1
		
	elif np.allclose(cycle[0], raster):
		# inicio de un nuevo ciclo
		cycles_sizes.append(cycle_size)
		cycle_size = 1
		
	elif cycle_size < len(cycle):
		# Revision de parte del ciclo ya descubierto
		cycle_size = cycle_size + 1
	else:
		# Parte nueva del ciclo descubierta
		cycle.append(raster)
		cycle_size = cycle_size + 1
	
	n = n+1
	line1 = f.readline()
	
f.close()


cycles_sizes = np.array(cycles_sizes)
size = cycles_sizes.shape[0]
mean = np.mean(cycles_sizes)
desv  = np.std(cycles_sizes)

print(inputs_filename + ";{};{};{}".format(size, mean, desv))
