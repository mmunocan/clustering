# Given a temporal raster base, generate a new dataset by repeating the first rasters until complete <n_rasters> rasters. The number of repeated rasters ranges from 1 to <cycle_size>

import sys
import numpy as np
import math

if len(sys.argv) != 8:
	print("ERROR! USE " + sys.argv[0] + " <inputs_filename> <inputs_path> <outputs_filename> <outputs_path> <n_rasters> <outputs_rasters_initial_filename> <cycle_size> ")
	sys.exit()

inputs_filename = sys.argv[1]
inputs_path = sys.argv[2]
outputs_filename = sys.argv[3]
outputs_path = sys.argv[4]
n_rasters = int(sys.argv[5])
outputs_rasters_initial_filename = sys.argv[6]
cycle_size = int(sys.argv[7])

f = open(inputs_filename, "r")
line1 = f.readline().split()
n_rows = int(line1[0])
n_cols = int(line1[1])

raster_type = np.dtype((np.int32, n_cols * n_rows))
cycle = []

# Lectura del dataset

for i in range(cycle_size):
	line1 = f.readline()
	raster_filename = inputs_path + "/" + line1
	raster_filename = raster_filename[:-1]
	file_raster = open(raster_filename, "rb")
	
	raster = np.fromfile(file_raster, dtype=raster_type)[0]
	file_raster.close()
	cycle.append(raster)
	
f.close()

# Repeticion del ciclo

output_file = open(outputs_filename, "w")
first_line = str(n_rows)+" "+str(n_cols)+" 4 2 3 2"
output_file.write(first_line+"\n")

time = 0
while time < n_rasters:
	
	semi_cycle = np.random.randint(cycle_size+1)
	
	for j in range(semi_cycle):
		if time < n_rasters:
			output_raster_filename = outputs_rasters_initial_filename + "_" + str(time) + ".bin"
			output_file.write(output_raster_filename+"\n")
			output_raster_filename = outputs_path + "/" + output_raster_filename

			raster_output = open(output_raster_filename, "wb")
			cycle[j].tofile(raster_output)
			raster_output.close()

			time = time + 1
			
output_file.close()

print("dataset " + outputs_filename + " ready!")