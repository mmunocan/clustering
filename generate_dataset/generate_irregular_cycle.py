# Generates a dataset whose first <cycle_size> rasters rasters contains random values from 1 to 100,  which are repeated cyclically until 
# to complete <n_rasters> rasters. Irregularity is based on generating semi-cycles, where only part of the cycle is repeated.

import sys
import numpy as np
import math

if len(sys.argv) != 8:
	print("ERROR! USE " + sys.argv[0] + " <outputs_filename> <outputs_path> <n_rasters> <n_rows> <n_cols> <outputs_rasters_initial_filename> <cycle_size> ")
	sys.exit()

outputs_filename = sys.argv[1]
outputs_path = sys.argv[2]
n_rasters = int(sys.argv[3])
n_rows = int(sys.argv[4])
n_cols = int(sys.argv[5])
outputs_rasters_initial_filename = sys.argv[6]
cycle_size = int(sys.argv[7])

original_cycle = np.zeros((cycle_size, n_rows, n_cols), dtype=np.int32)

# Generacion del primer ciclo
for i in range(cycle_size):
	original_cycle[i] = np.random.randint(100, size=(n_rows, n_cols))

# ReplicaciÃ³n del ciclo

output_file = open(outputs_filename, "w")
first_line = str(n_rows)+" "+str(n_cols)+" 4 2 3 2"
output_file.write(first_line+"\n")

time = 0
while time < n_rasters:
	
	semi_cycle = np.random.randint(cycle_size+1)
	
	for j in range(semi_cycle):
		
		if time < n_rasters:
		
			output_raster_filename = outputs_rasters_initial_filename + "_" + str(time) + ".bin"
			output_file.write(output_raster_filename+"\n")
			output_raster_filename = outputs_path + "/" + output_raster_filename

			raster_output = open(output_raster_filename, "wb")
			original_cycle[j].tofile(raster_output)
			raster_output.close()

			time = time + 1
			
output_file.close()

print("dataset " + outputs_filename + " ready!")