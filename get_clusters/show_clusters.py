# Show the cluster file values

import sys
import numpy as np

if len(sys.argv) != 2:
	print("ERROR! USE " + sys.argv[0] + " <cluster_filename>")
	sys.exit()

cluster_filename = sys.argv[1]

# Reading the cluster file
f = open(cluster_filename, "rb")
clusteres = np.fromfile(f, dtype=np.int32)
f.close()
n_rasters = clusteres.shape[0]

# Shows the results
for i in range(n_rasters):
	print(str(clusteres[i]))
	
unique = len(np.unique(clusteres))
print("n_clusters: " + str(unique))
