# Given a cluster file, modify the cluster ID with its raster centroid position defined as 
# the best centroid.
# The best centroid that minimizes the sum of the distances between this raster and the
# rest of raster inside the cluster.

import sys
import numpy as np
import math

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> <cluster_filename> <output_filename>")
	sys.exit()

matrix_filename = sys.argv[1]
cluster_filename = sys.argv[2]
output_filename = sys.argv[3]

print("Centroid best " + output_filename)

# Reading the distance matrix
f = open(matrix_filename, "rb")
distance_matrix = np.fromfile(f, dtype=np.float32)
f.close()
n_rasters = (int)(math.sqrt(distance_matrix.shape[0]))
distance_matrix = np.resize(distance_matrix, (n_rasters, n_rasters))

# Reading the cluster file
f = open(cluster_filename, "rb")
clusteres = np.fromfile(f, dtype=np.int32)
f.close()
n_clusters = clusteres.max() + 1

# Select the best centroid
# centroids[i]: best centroid position of the raster in position i
centroids = []
# For each cluster
for id_cluster in range(n_clusters):
	# Identify the rasters inside the cluster
	cluster_group = []
	for i in range(n_rasters):
		if(clusteres[i] == id_cluster):
			cluster_group.append(i)
	# Searching the best centroid
	centroid = -1
	total_distance = 250**250
	
	for candidate in cluster_group:
		distance = 0
		for other_point in cluster_group:
			distance = distance + distance_matrix[candidate][other_point]
		if(distance < total_distance):
			total_distance = distance
			centroid = candidate
			
	# Storing the best centroid
	centroids.append(centroid)

# Reeplace the clusters ID
output = []
for i in range(n_rasters):
	if clusteres[i] >= n_clusters:
		# The ID is out of range
		print("Cluster position " + str(i) + " presents an ID = " + str(clusteres[i]) + " greater tan " + str(n_clusters) + " allowed")
		sys.exit()
	output.append(centroids[clusteres[i]])

# Writing the output
output = np.array(output, dtype=np.int32)	
file_output = open(output_filename, "wb")
output.tofile(file_output)
file_output.close()