# Computing clustering technique on rasters time series

This project generate the clusters rasters inside a temporal raster, for a consecutive use in the construction of a $T$-$k^2$-raster. The clusterization uses differents configurations, such the strategy used (Hierarchical or Kmeans++) or centroid selection (best raster or first raster).

This project receives the distences matrices as a binary file that contains the values as a 32 bits floats numbers, and generates a cluster file as a binary file that contains <n_rasters> integers of 32 bits. This file can contains the clusters ID without identfy the raster centroid or the position of the raster centroid for each cluster (such a $cs\_v$ vector). This file is a binary that contains <n_rasters> 32 bits integers.

## Libraries used

* Python: sys, numpy, sklearn.cluster, sklearn_extra.cluster, sklearn.preprocessing, sklearn.metrics


## Details

### Application of hierarchical clustering

Given a distance matrix, for to apply hierarchical clustering use:

``````
python3 get_clusters_hierarchical.py <matrix_filename> <n_clusteres> <output_filename>
``````

Where:
* <matrix_filename>: Matrix Distance Filename
* <n_clusteres>: Number of clusters to generate
* <output_filename>: Cluster filename

For clustering application, we use the following configuration:

``````
AgglomerativeClustering(metric = 'precomputed',linkage  = 'complete',n_clusters = n_clusters)
``````

### Application of kmedods clustering

Given a distance matrix, for to apply kmedoids clustering use:

``````
python3 get_clusters_kmedoids.py <matrix_filename> <n_clusteres> <output_filename>
``````

Where:
* <matrix_filename>: Matrix Distance Filename
* <n_clusteres>: Number of clusters to generate
* <output_filename>: Cluster filename

For clustering application, we use the following configuration:

``````
KMedoids(metric = 'precomputed', init  = 'k-medoids++',n_clusters = n_clusters)
``````

### Selection of best centroids

Given a cluster file, generate a new cluster file which each cluster ID is reeplaced with the ID of the best cluster selected. For the best cluster selection, we choose the raster with the minimum sum of the distances between each itself and the rest of the raster inside the cluster. For this computation, we need the distance matrix.

Use:
``````
python3 get_centroid_best.py <matrix_filename> <cluster_filename> <output_filename>
``````

Where:
* <matrix_filename>: Matrix Distance Filename
* <cluster_filename>: Cluster filename
* <output_filename>: New cluster filename

### Selection of first centroids

Given a cluster file, generate a new cluster file which each cluster ID is reeplaced with the ID of the first cluster selected. For this, we asociate each cluster ID with the ID of the first raster that appear with the same cluster ID in a time-order. Next, we reeplace each cluster ID with the found raster ID.

Use:
``````
python3 get_centroid_first.py <cluster_filename> <output_filename>
``````

Donde:
* <cluster_filename>: Cluster filename
* <output_filename>: New cluster filename

### Show a cluster file content

Given a cluster file, for to show its content, use:
``````
python3 show_clusters.py <cluster_filename> 
``````

Donde:
* <cluster_filename>: Cluster filename

### Selection of ideal number of clusters for hierarchical clustering

Given a distance matrix, the script compute the Silhouette Index for each possible cluster amount between 2 and <n_rasters>, reporting the value with the highter Silhouette Index. Use:

``````
python3 get_ideal_cluster_hierarchical.py <matrix_filename>
``````

Where:
* <matrix_filename>: Matrix Distance Filename

### Selection of ideal number of clusters for kmedoids clustering

Given a distance matrix, the script compute the Silhouette Index for each possible cluster amount between 2 and <n_rasters>, reporting the value with the highter Silhouette Index. Use:
``````
python3 get_ideal_cluster_kmedoids.py <matrix_filename>
``````

Where:
* <matrix_filename>: Matrix Distance Filename

### Compute the Silhouette Index

Given a cluster file and a distance matrix, compute the silhouette index for that cluster file.

Use:
``````
python3 get_silhouette_value.py <matrix_filename> <cluster_filename>
``````

Where:
* <matrix_filename>: Matrix Distance Filename
* <cluster_filename>: Cluster filename

