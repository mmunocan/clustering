# Given a cluster file, modify the cluster ID with its raster centroid position defined as 
# the first centroid.
# The best centroid is the first raster of the cluster that appears in a time order.

import sys
import numpy as np

if len(sys.argv) != 3:
	print("ERROR! USE " + sys.argv[0] + " <cluster_filename> <output_filename>")
	sys.exit()

cluster_filename = sys.argv[1]
output_filename = sys.argv[2]

print("Centroid first " + output_filename)

# Reading the cluster file
f = open(cluster_filename, "rb")
clusteres = np.fromfile(f, dtype=np.int32)
f.close()
n_rasters = clusteres.shape[0]

# Dicctionary that associate the first raster position for each cluster
centroids_representant = {}
# Select the best centroid for each cluster
centroids = []

for i in range(n_rasters):
	if centroids_representant.__contains__(clusteres[i]):
		# If the cluster ID appeared, we assign the raster position associated in the dictionary
		centroids.append(centroids_representant[clusteres[i]][0])
	else:
		# If is the first apparition of the cluster ID, is registered in the dictionary and
		# includes its assignation
		centroids_representant[clusteres[i]] = [i]
		centroids.append(centroids_representant[clusteres[i]][0])

# Writing the output
centroids = np.array(centroids, dtype=np.int32)
file_output = open(output_filename, "wb")
centroids.tofile(file_output)
file_output.close()