import sys
import numpy as np
import math

from sklearn_extra.cluster import KMedoids

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> <n_clusters> <output_filename>")
	sys.exit()

matrix_filename = sys.argv[1]
n_clusters = int(sys.argv[2])
output_filename = sys.argv[3]

print("Kmedoids clustering " + output_filename + " with " + str(n_clusters) + " clusters")

# Reading the distance matrix
f = open(matrix_filename, "rb")
distance_matrix = np.fromfile(f, dtype=np.float32)
f.close()
n_rasters = (int)(math.sqrt(distance_matrix.shape[0]))
distance_matrix = np.resize(distance_matrix, (n_rasters, n_rasters))

# Computing the clusterization
model = KMedoids(metric = 'precomputed', init  = 'k-medoids++',n_clusters = n_clusters)
clusteres = model.fit(distance_matrix).labels_
clusteres = np.array(clusteres, dtype=np.int32)

# Writing the output
file_output = open(output_filename, "wb")
clusteres.tofile(file_output)
file_output.close()