import sys
import numpy as np
import math

from sklearn.cluster import AgglomerativeClustering

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> <n_clusters> <output_filename>")
	sys.exit()

matrix_filename = sys.argv[1]
n_clusters = int(sys.argv[2])
output_filename = sys.argv[3]

print("Hierarchical clustering " + output_filename + " with " + str(n_clusters) + " clusters")

# Reading the distance matrix
f = open(matrix_filename, "rb")
distance_matrix = np.fromfile(f, dtype=np.float32)
f.close()
n_rasters = (int)(math.sqrt(distance_matrix.shape[0]))
distance_matrix = np.resize(distance_matrix, (n_rasters, n_rasters))

# Computing the clusterization
model = AgglomerativeClustering(metric = 'precomputed',linkage  = 'complete',n_clusters = n_clusters)
clusteres = model.fit(distance_matrix).labels_
clusteres = np.array(clusteres, dtype=np.int32)	

# Writing the output
file_output = open(output_filename, "wb")
clusteres.tofile(file_output)
file_output.close()