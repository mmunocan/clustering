import sys
import numpy as np
import math

from sklearn_extra.cluster import KMedoids
from sklearn.preprocessing import scale
from sklearn.metrics import silhouette_score

if len(sys.argv) != 2:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> ")
	sys.exit()

matrix_filename = sys.argv[1]

# Reading the distance matrix
f = open(matrix_filename, "rb")
distance_matrix = np.fromfile(f, dtype=np.float32)
f.close()
n_rasters = (int)(math.sqrt(distance_matrix.shape[0]))
distance_matrix = np.resize(distance_matrix, (n_rasters, n_rasters))

# Getting silhouette 
silhouette = []
clusteres = range(2, n_rasters, 2)
for cant_clusters in clusteres:
	model = KMedoids(metric = 'precomputed', init  = 'k-medoids++', n_clusters = cant_clusters)
	cluster_labels = model.fit_predict(distance_matrix)
	silhouette_avg = silhouette_score(distance_matrix, cluster_labels)
	silhouette.append(silhouette_avg)

# Getting cluster number with the best silhouette score
ideal_cluster = 0
silhouette_avg = -1
for i in range(len(clusteres)):
	if silhouette[i] > silhouette_avg:
		ideal_cluster = clusteres[i]
		silhouette_avg = silhouette[i]

print(matrix_filename + " {} {}".format(ideal_cluster, silhouette_avg))