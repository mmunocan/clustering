import sys
import numpy as np
import math

from sklearn.metrics import silhouette_score

if len(sys.argv) != 3:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> <cluster_filename> ")
	sys.exit()

matrix_filename = sys.argv[1]
cluster_filename = sys.argv[2]

# Reading the distance matrix
f = open(matrix_filename, "rb")
distance_matrix = np.fromfile(f, dtype=np.float32)
f.close()
n_rasters = (int)(math.sqrt(distance_matrix.shape[0]))
distance_matrix = np.resize(distance_matrix, (n_rasters, n_rasters))

# Reading the cluster file
f = open(cluster_filename, "rb")
cluster_labels = np.fromfile(f, dtype=np.int32)
f.close()


# Computing the silhouette index
silhouette_avg = silhouette_score(distance_matrix, cluster_labels)

print(matrix_filename + " {}".format(silhouette_avg))

