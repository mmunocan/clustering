# Generation of distances matrix

This project generates the different matrix disstances on a temporal raster using WHD and CHD. The output is used as a input in **clustering** project.

This project recives the temporal rasters datasets compatible with $T$-$k^2$-raster input and generate a binary file with <n_rasters>x<n_rasters> float of 32 bits. For each generated matrix, the script report the generation time in ms. For reduce the generation time, the project uses the C++ library **omp.h** for a parallel execution.

Additionally, it is possible to generate a heat map graphic for to observe the values inside the distance matrix.

## Libraries used

* C++: iostream, chrono, omp.h, fstream, cmath, cassert, vector
* Python: sys, numpy, matplotlib

## Details

### Generating a distance matrix

Given a dataset, for to generate a distance matrix, use:


``````
./<distance_name> <inputs_file> <inputs_path> <n_rasters> <report_filename>
``````

Where:
* <distance_name>; Name of the distance to use. It can be **NHD**, **WHD** o **CHD**
* <inputs_file> <inputs_path>: input identification
* <n_rasters>: Number of raster inside the input dataset
* <report_filename>: Matrix Distance Filename

### Generating the heat map

Given a distance matrix, for to generate a heat map graphic, use:


``````
python3 heat_map.py <matrix_filename> <n_rasters> <graphic_filename>
``````

Donde:
* <matrix_filename>: Matrix Distance Filename
* <n_rasters>: Number of raster inside the matrix distance
* <graphic_filename>: Output graphic filename. It is recommendable to include **.png** extension


