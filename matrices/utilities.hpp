#ifndef UT_HPP
#define UT_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <cassert>
#include <climits>

using namespace std;

void read_dataset(string inputs_file, string inputs_path, size_t n_rasters, vector<vector<int>> & rasters);
void write_output(string report_filename, size_t n_rasters, float * results);
float get_WHD(vector<int> & rast1, vector<int> & rast2);
float get_NHD(vector<int> & rast1, vector<int> & rast2);
float get_jaccard(vector<int> & rast1, vector<int> & rast2, int minimum);
int get_minimum_value(vector<vector<int>> & rasters);

#endif