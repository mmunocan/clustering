import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <matrix_filename> <n_rasters> <graphic_filename>")
	sys.exit()

matrix_filename = sys.argv[1]
n_rasters = int(sys.argv[2])
graphic_filename = sys.argv[3]

f = open(matrix_filename, "rb")
matrix_format = np.dtype((np.float32, (n_rasters,n_rasters)))
data = np.fromfile(f, dtype=matrix_format)
f.close()

fig, ax = plt.subplots()
im = ax.imshow(data[0], cmap = "autumn")
ax.set_ylabel("Raster")
ax.set_xlabel("Raster")

cbar = ax.figure.colorbar(im, ax = ax)
cbar.ax.set_ylabel("Color bar", rotation = -90, va = "bottom")

plt.tight_layout()
plt.savefig(graphic_filename)
