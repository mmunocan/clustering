/*
 * Compute the Weighted Hamming Distance
 */

#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <omp.h>

#include "utilities.hpp"

using namespace std;

int main(int argc, char ** argv){
	if(argc != 5){
		cout << "ERROR! USE: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters> <report_filename>" << endl;
		return -1;
	}
	
	/**********************************/
	/* OBTAINING THE INPUT PARAMETERS */
	/**********************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_rasters = atoi(argv[3]);
	string report_filename = argv[4];
	
	/*****************************/
	/* READING THE INPUT DATASET */
	/*****************************/
	vector<vector<int>> rasters(n_rasters);
	read_dataset(inputs_file, inputs_path, n_rasters, rasters);
	
	/*************************/
	/* GENERATING THE REPORT */
	/*************************/
	auto start = chrono::high_resolution_clock::now();
	
	float * results = new float[n_rasters*n_rasters];
	#pragma omp parallel for
	for(size_t rast1 = 0; rast1 < n_rasters-1; rast1++){
		
		for(size_t rast2 = rast1+1; rast2 < n_rasters; rast2++){
			results[rast1*n_rasters+rast2] = get_WHD(rasters[rast1], rasters[rast2]);
			results[rast2*n_rasters+rast1] = results[rast1*n_rasters+rast2];
		}
	}
	auto finish = chrono::high_resolution_clock::now();
	long time = chrono::duration_cast<chrono::milliseconds> (finish - start).count();
	
	cout << inputs_file << " " << time << " [ms]" << endl;
	
	/**********************/
	/* WRITING THE MATRIX */
	/**********************/
	write_output(report_filename, n_rasters, results);
	
	
	return 0;
}