#include "utilities.hpp"

void read_dataset(string inputs_filename, string inputs_path_folder, size_t n_rasters, vector<vector<int>> & rasters){
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	
	string raster_input_filename;
	int value;
	
	for(size_t time = 0; time < n_rasters; time++){
		rasters[time].resize(n_rows * n_cols);
		
		inputs_file >> raster_input_filename;
		raster_input_filename = inputs_path_folder + "/" + raster_input_filename;
		
		ifstream raster_input_file(raster_input_filename);
		assert(raster_input_file.is_open() && raster_input_file.good());
		
		for(size_t c = 0; c < (n_rows * n_cols); c++){
			raster_input_file.read((char *) (& value), sizeof(int));
			rasters[time][c] = value;
		}
		raster_input_file.close();
	}
	inputs_file.close();
}

void write_output(string report_filename, size_t n_rasters, float * results){
	ofstream report(report_filename);
	float value_float;
	for(size_t rast1 = 0; rast1 < n_rasters; rast1++){
		for(size_t rast2 = 0; rast2 < n_rasters; rast2++){
			value_float = results[rast1*n_rasters+rast2];
			report.write((char *) (& value_float), sizeof(float));
		}
	}
	
	report.close();
}

float get_WHD(vector<int> & rast1, vector<int> & rast2){
	float result = 0;
	size_t size = rast1.size();
	
	for(size_t cell = 0; cell < size; cell++){
		result += abs(rast1[cell] - rast2[cell]);
	}
	
	result /= size;
	
	return result;
}

float get_NHD(vector<int> & rast1, vector<int> & rast2){
	float result = 0;
	size_t size = rast1.size();
	
	for(size_t cell = 0; cell < size; cell++){
		result += rast1[cell] != rast2[cell];
	}
	
	result /= size;
	
	return result;
}
