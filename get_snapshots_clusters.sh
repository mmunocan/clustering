#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <output_path> <list_files>"
	exit
fi

output_path=$1
list_files=$2

mkdir -p $output_path

i=0
while IFS= read -r line
do
	set -- $line
		input_files=$1
		input_path=$2
		output_id=$3
		
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-WHD-4-2-3-1.bin" "$output_path/$output_id-hierarchical-WHD-4-2-3-1-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-hierarchical-WHD-4-2-3-1.bin" "$output_path/$output_id-hierarchical-WHD-4-2-3-1-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-CHD-4-2-3-1.bin" "$output_path/$output_id-hierarchical-CHD-4-2-3-1-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-hierarchical-CHD-4-2-3-1.bin" "$output_path/$output_id-hierarchical-CHD-4-2-3-1-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-WHD-4-2-3-2.bin" "$output_path/$output_id-hierarchical-WHD-4-2-3-2-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-hierarchical-WHD-4-2-3-2.bin" "$output_path/$output_id-hierarchical-WHD-4-2-3-2-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-CHD-4-2-3-2.bin" "$output_path/$output_id-hierarchical-CHD-4-2-3-2-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-hierarchical-CHD-4-2-3-2.bin" "$output_path/$output_id-hierarchical-CHD-4-2-3-2-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-WHD-4-2-4-1.bin" "$output_path/$output_id-hierarchical-WHD-4-2-4-1-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-hierarchical-WHD-4-2-4-1.bin" "$output_path/$output_id-hierarchical-WHD-4-2-4-1-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-CHD-4-2-4-1.bin" "$output_path/$output_id-hierarchical-CHD-4-2-4-1-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-hierarchical-CHD-4-2-4-1.bin" "$output_path/$output_id-hierarchical-CHD-4-2-4-1-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-WHD-4-2-4-2.bin" "$output_path/$output_id-hierarchical-WHD-4-2-4-2-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-hierarchical-WHD-4-2-4-2.bin" "$output_path/$output_id-hierarchical-WHD-4-2-4-2-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-CHD-4-2-4-2.bin" "$output_path/$output_id-hierarchical-CHD-4-2-4-2-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-hierarchical-CHD-4-2-4-2.bin" "$output_path/$output_id-hierarchical-CHD-4-2-4-2-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-WHD-4-2-3-1.bin" "$output_path/$output_id-kmedoids-WHD-4-2-3-1-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-kmedoids-WHD-4-2-3-1.bin" "$output_path/$output_id-kmedoids-WHD-4-2-3-1-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-CHD-4-2-3-1.bin" "$output_path/$output_id-kmedoids-CHD-4-2-3-1-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-kmedoids-CHD-4-2-3-1.bin" "$output_path/$output_id-kmedoids-CHD-4-2-3-1-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-WHD-4-2-3-2.bin" "$output_path/$output_id-kmedoids-WHD-4-2-3-2-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-kmedoids-WHD-4-2-3-2.bin" "$output_path/$output_id-kmedoids-WHD-4-2-3-2-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-CHD-4-2-3-2.bin" "$output_path/$output_id-kmedoids-CHD-4-2-3-2-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-kmedoids-CHD-4-2-3-2.bin" "$output_path/$output_id-kmedoids-CHD-4-2-3-2-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-WHD-4-2-4-1.bin" "$output_path/$output_id-kmedoids-WHD-4-2-4-1-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-kmedoids-WHD-4-2-4-1.bin" "$output_path/$output_id-kmedoids-WHD-4-2-4-1-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-CHD-4-2-4-1.bin" "$output_path/$output_id-kmedoids-CHD-4-2-4-1-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-kmedoids-CHD-4-2-4-1.bin" "$output_path/$output_id-kmedoids-CHD-4-2-4-1-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-WHD-4-2-4-2.bin" "$output_path/$output_id-kmedoids-WHD-4-2-4-2-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-kmedoids-WHD-4-2-4-2.bin" "$output_path/$output_id-kmedoids-WHD-4-2-4-2-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-CHD-4-2-4-2.bin" "$output_path/$output_id-kmedoids-CHD-4-2-4-2-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-kmedoids-CHD-4-2-4-2.bin" "$output_path/$output_id-kmedoids-CHD-4-2-4-2-best.bin"
		
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-WHD-ideal.bin" "$output_path/$output_id-hierarchical-WHD-ideal-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-hierarchical-WHD-ideal.bin" "$output_path/$output_id-hierarchical-WHD-ideal-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-hierarchical-CHD-ideal.bin" "$output_path/$output_id-hierarchical-CHD-ideal-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-hierarchical-CHD-ideal.bin" "$output_path/$output_id-hierarchical-CHD-ideal-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-WHD-ideal.bin" "$output_path/$output_id-kmedoids-WHD-ideal-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-WHD.bin" "$output_path/$output_id-kmedoids-WHD-ideal.bin" "$output_path/$output_id-kmedoids-WHD-ideal-best.bin"
		python3 get_centroid_first.py "$output_path/$output_id-kmedoids-CHD-ideal.bin" "$output_path/$output_id-kmedoids-CHD-ideal-first.bin"
		python3 get_centroid_best.py "matrices/$output_id-CHD.bin" "$output_path/$output_id-kmedoids-CHD-ideal.bin" "$output_path/$output_id-kmedoids-CHD-ideal-best.bin"
		
		i=$(($i + 1))
done < $list_files